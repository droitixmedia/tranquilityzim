@extends($activeTemplate.'layouts.masternew')
@section('content')

  <div class="page-content-wrapper">
      <div class="container">
        <!-- Support Wrapper-->
        <div class="support-wrapper py-3">
          <div class="card">
            <div class="card-body">
              <h4 class="faq-heading text-center">Support Center</h4>
              <!-- Search Form-->
              <form class="faq-search-form" action="#" method="">
                <input class="form-control" type="search" name="search" placeholder="Search">
                <button type="submit"><i class="fa fa-search"></i></button>
              </form>
            </div>
          </div>
          <!-- Accordian Area Wrapper-->
          <div class="accordian-area-wrapper mt-3">
            <!-- Accordian Card-->
            <div class="card accordian-card">
              <div class="card-body">
                <h5 class="accordian-title">General Issues</h5>
                <div class="accordion" id="accordion1">
                  <!-- Single Accordian-->
                  <div class="accordian-header" id="headingOne">

                    <button class="d-flex align-items-center justify-content-between w-100 collapsed btn" type="button"  aria-expanded="false" aria-controls="collapseOne"><span><i class="fa-solid fa-phone"></i> Call Center One</span><a href="https://wa.me/+2609782824644?text=Hello Call Center 1."><i class="fa-brands fa-whatsapp  lni-tada-effect"></i> Whatsapp Now</a></button>

                  </div>

                  <!-- Single Accordian-->
                  <div class="accordian-header" id="headingTwo">

                    <button class="d-flex align-items-center justify-content-between w-100 collapsed btn" type="button"  aria-expanded="false" aria-controls="collapseOne"><span><i class="fa-solid fa-phone"></i> Call Center Two</span><a href="https://wa.me/+2607739919541?text=Hello Call Center 2."><i class="fa-brands fa-whatsapp  lni-tada-effect"></i> Whatsapp Now</a></button>

                  </div>

                </div>
              </div>
            </div>
          </div>
          <!-- Accordian Area Wrapper-->
          <div class="accordian-area-wrapper mt-3">
            <!-- Accordian Card-->
            <div class="card accordian-card seller-card">
              <div class="card-body">
                <h5 class="accordian-title">Deposits & Withdrawals</h5>
                <div class="accordion" id="accordion2">
                  <!-- Single Accordian-->
                  <div class="accordian-header" id="headingThree">

                    <button class="d-flex align-items-center justify-content-between w-100 collapsed btn" type="button"  aria-expanded="false" aria-controls="collapseThree"><span><i class="fa-solid fa-cloud-arrow-up"></i>Deposits Queries</span><a href="https://wa.me/+260762799409?text=Hello Deposit Manager."><i class="fa-brands fa-whatsapp  lni-tada-effect"></i> Whatsapp Now</a></button>
                  </div>

                  <!-- Single Accordian-->
                  <div class="accordian-header" id="headingFour">
                    <button class="d-flex align-items-center justify-content-between w-100 collapsed btn" type="button"  aria-expanded="false" aria-controls="collapseFour"><span><i class="fa-solid fa-dollar-sign"></i>Withdrawal Queries</span><a href="https://wa.me/+260762799409?text=Hello Withdrawal Manager."><i class="fa-brands fa-whatsapp  lni-tada-effect"></i> Whatsapp Now</a></button>
                  </div>

                </div>
              </div>
            </div>
          </div>
          <!-- Accordian Area Wrapper-->
          <div class="accordian-area-wrapper mt-3">
            <!-- Accordian Card-->
            <div class="card accordian-card others-card">
              <div class="card-body">
                <h5 class="accordian-title">Company Documents & Downloads</h5>
                <div class="accordion" id="accordion3">
                  <!-- Single Accordian-->
                  <div class="accordian-header" id="headingFive">
                    <button class="d-flex align-items-center justify-content-between w-100 collapsed btn" type="button"  aria-expanded="false" aria-controls="collapseFive"><span><i class="fa-solid fa-file"></i>PACRA REGISTRATION CERTIFICATE</span><a href="https://doorstep.pro/doorstep.pdf" download><i class="fa-solid fa-download"></i> Download</a></button>
                  </div>

                  <!-- Single Accordian-->
                  <div class="accordian-header" id="headingSix">
                    <button class="d-flex align-items-center justify-content-between w-100 collapsed btn" type="button"  aria-expanded="false" aria-controls="collapseSix"><span><i class="fa-solid fa-file-invoice"></i> DOORSTEP DROPSHIPPING LICENCE</span><a href="#"><i class="fa-solid fa-download"></i> Download</a></button>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
