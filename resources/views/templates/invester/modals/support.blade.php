<!-- Modal Support Start-->
<div class="modal fade" id="supportModal" tabindex="-1" role="dialog" aria-labelledby="supportModalTitle" aria-hidden="true">
    <div class="modal-dialog mx-auto my-0 d-flex h-100 pt-5" role="document">
        <div class="modal-content mt-auto">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><img src="/transport/img/icons/ic-close.svg" alt="Icon Close"></span>
            </button>
            <div class="modal-middle">
                <div class="text-center change-image-theme-ib">
                    <img src="img/icons/support.svg" class="modal-icon" alt="Support Icon">
                    <img src="/transport/img/icons/support-dark-theme.svg" class="modal-icon dark-theme-image" alt="Support Icon">
                    <div class="modal-title">
                        Support 24/7
                    </div>
                </div>
                <div class="modal-text">
                    Support Agents work from 9AM TO 8.30PM so be patient during outside hours.
                </div>
            <div class="popups-menu-block d-flex justify-content-between align-items-center">
            <a href="https://wa.me/+263782458452?text=Good Day." class="popup-menu-item text-center" >
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/assets-protection.svg" alt="Withdraw Image">
                    <img width="30" src="/transport/img/whatsapp.svg" class="dark-theme-image" alt="Withdraw Image">
                </div>
                Dan(System Admin)
            </a>
            <a href="https://wa.me/+263782993146?text=Good Day." class="popup-menu-item text-center">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/deposit.svg" alt="Send Image">
                    <img width="30" src="/transport/img/whatsapp.svg" class="dark-theme-image" alt="Withdraw Image">
                </div>
                Patience(Support)
            </a>
            
           
        </div>

            </div>
            <div class="modal-bottom d-flex mt-4">
                <a class="btn btn-light w-100" data-dismiss="modal">CLOSE</a>
               
            </div>
        </div>
    </div>
</div>
<!-- Modal Support End-->