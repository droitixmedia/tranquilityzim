<!-- Modal Add Money Start-->
<div class="modal fade" id="balanceModal" tabindex="-1" role="dialog" aria-labelledby="balanceModalTitle" aria-hidden="true">
    <div class="modal-dialog mx-auto my-0 d-flex h-100 pt-5" role="document">
        <div class="modal-content mt-auto">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><img src="/transport/img/icons/ic-close.svg" alt="Icon Close"></span>
            </button>
            <div class="modal-middle">
                <div class="text-center change-image-theme-ib">
                    <img src="/transport/img/icons/add-money-icon.svg" class="modal-icon" alt="Add Money Icon">
                    <img src="/transport/img/icons/add-money-icon-dark-theme.svg" class="modal-icon dark-theme-image" alt="Add Money Icon">
                    <div class="modal-title">
                        Add Balance
                    </div>
                </div>
                <div class="modal-text">
                    Certe, inquam, pertinax non intellegamus, tu paulo ante cum memoriter, tum etiam erga nos causae confidere, sed quia non numquam eius, quid.
                </div>

                <div class="devider-without-line"></div>

                <!-- Add Balance Form Start -->
                <form class="add-balance-form" action="{{route('user.deposit.insert')}}" method="post">
                    @csrf
                     
                
                    <!-- Form Field Start -->
                    <div class="d-flex align-items-center">
                        <label for="balance-from">From</label>
                        <input class="form-control" type="text" autocomplete="off" name="balance-from" id="balance-from" placeholder="PayPal Account">
                    </div>
                    <!-- Form Field End -->          
        
                    <!-- Form Field Start -->
                    <div class="d-flex align-items-center">
                        <label for="balance-amount">Enter Amount</label>
                        <input class="form-control" type="text" autocomplete="off" name="balance-amount" id="balance-amount" placeholder="$100">
                    </div>
                    <!-- Form Field End -->
                </form>
                <!-- Add Balance Form End -->

            </div>
            <div class="modal-bottom d-flex mt-4">
                <a class="btn btn-light w-100" data-dismiss="modal">CANCEL</a>
                <a class="btn btn-dark w-100 ml-2">DEPOSIT</a>
            </div>
        </div>
    </div>
</div>
<!-- Modal Add Money End-->