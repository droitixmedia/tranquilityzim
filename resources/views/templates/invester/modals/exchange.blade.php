<!-- Modal Support Start-->
<div class="modal fade" id="transactionsModal" tabindex="-1" role="dialog" aria-labelledby="supportModalTitle" aria-hidden="true">
    <div class="modal-dialog mx-auto my-0 d-flex h-100 pt-5" role="document">
        <div class="modal-content mt-auto">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><img src="/transport/img/icons/ic-close.svg" alt="Icon Close"></span>
            </button>
            <div class="modal-middle">
                <div class="text-center change-image-theme-ib">
                   
                    <div class="modal-title">
                       Transactions
                    </div>
                </div>
<div class="popups-menu-block d-flex justify-content-between align-items-center">
            <a href="{{ url('user/withdraw/history') }}" class="popup-menu-item text-center" >
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/assets-protection.svg" alt="Withdraw Image">
                    <img width="30" src="/transport/img/withdrawal.svg" class="dark-theme-image" alt="Withdraw Image">
                </div>
                Withdrawals
            </a>
            <a href="{{ url('user/deposit/history') }}" class="popup-menu-item text-center">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/deposit.svg" alt="Send Image">
                    <img width="30" src="/transport/img/deposit.svg" class="dark-theme-image" alt="Send Image">
                </div>
                Deposits
            </a>
            <a href="{{ route('user.referrals') }}" class="popup-menu-item text-center">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/exchange.svg" alt="Exchange Image">
                    <img width="30" src="/transport/img/bonus.svg" class="dark-theme-image" alt="Exchange Image">
                </div>
                Referral
            </a>
            <a href="{{ route('user.transactions') }}" class="popup-menu-item text-center">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/support.svg" alt="Support Image">
                    <img src="/transport/img/icons/transactions-dark-theme.svg" class="dark-theme-image" alt="Support Image">
                </div>
                All History
            </a>
        </div>

            </div>
            <div class="modal-bottom d-flex mt-4">
                <a class="btn btn-light w-100" data-dismiss="modal">CLOSE</a>
               
            </div>
        </div>
    </div>
</div>
<!-- Modal Support End-->