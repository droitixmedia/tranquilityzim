<!-- Modal Withdraw Start-->
<div class="modal fade" id="withdrawModal" tabindex="-1" role="dialog" aria-labelledby="withdrawModalTitle" aria-hidden="true">
    <div class="modal-dialog mx-auto my-0 d-flex h-100 pt-5" role="document">
        <div class="modal-content mt-auto">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><img src="/transport/img/icons/ic-close.svg" alt="Icon Close"></span>
            </button>
            <div class="modal-middle">
                <div class="text-center change-image-theme-ib">
                    <img src="/transport/img/icons/assets-protection.svg" class="modal-icon" alt="Withdraw Icon">
                    <img src="/transport/img/icons/assets-protection-dark-theme.svg" class="modal-icon dark-theme-image" alt="Withdraw Icon">
                    <div class="modal-title">
                        Withdraw
                    </div>
                </div>
                <div class="modal-text">
                    Certe, inquam, pertinax non intellegamus, tu paulo ante cum memoriter, tum etiam erga nos causae confidere, sed quia non numquam eius modi.
                </div>

                <div class="devider-without-line"></div>

                <!-- Withdraw Form Start -->
                <form class="withdraw-form">
                    <!-- Form Field Start -->
                    <div class="d-flex align-items-center">
                        <label for="withdraw-from">From</label>
                        <select name="withdraw-from" id="withdraw-from">
                            <option value="Savings (***5411)">Savings (***5411)</option>
                            <option value="Savings (***2311)">Savings (***2311)</option>
                            <option value="Savings (***5511)">Savings (***5511)</option>
                            <option value="Savings (***5561)">Savings (***5561)</option>
                        </select>
                    </div>
                    <!-- Form Field End --> 
                    
                    <!-- Form Field Start -->
                    <div class="d-flex align-items-center">
                        <label for="withdraw-to">To</label>
                        <div class="card-image">
                            <img src="/transport/img/icons/mastercard.svg" alt="MasterCard Icon">
                        </div>
                        <select name="withdraw-to" id="withdraw-to">
                            <option value="**** **** **** 5324">
                                **** **** **** 5324
                            </option>
                            <option value="**** **** **** 5351">
                                **** **** **** 5351
                            </option>
                            <option value="**** **** **** 1124">
                                **** **** **** 1124
                            </option>
                        </select>
                    </div>
                    <!-- Form Field End -->
        
                    <!-- Form Field Start -->
                    <div class="d-flex align-items-center">
                        <label for="withdraw-amount">Enter Amount</label>
                        <input class="form-control" type="text" autocomplete="off" name="withdraw-amount" id="withdraw-amount" placeholder="$100">
                    </div>
                    <!-- Form Field End -->
                </form>
                <!-- Withdraw Form End -->

            </div>
            <div class="modal-bottom d-flex mt-4">
                <a class="btn btn-light w-100" data-dismiss="modal">CANCEL</a>
                <a class="btn btn-dark w-100 ml-2">WITHDRAW</a>
            </div>
        </div>
    </div>
</div>
<!-- Modal Withdraw End-->