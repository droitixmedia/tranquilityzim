<!-- Modal Send Money Start-->

@php
use App\Models\GatewayCurrency;

$gatewayCurrency = GatewayCurrency::with(['method' => function ($query) {
  $query->where('status', 1);
}])->whereHas('method', function ($gate) {
  $gate->where('status', 1);
})->orderBy('name')->get();
@endphp
<div class="modal fade" id="sendMoneyModal" tabindex="-1" role="dialog" aria-labelledby="sendMoneyModalTitle" aria-hidden="true">
    <div class="modal-dialog mx-auto my-0 d-flex h-100 pt-5" role="document">
        <div class="modal-content mt-auto">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><img src="/transport/img/icons/ic-close.svg" alt="Icon Close"></span>
            </button>
            <div class="modal-middle">
                <div class="text-center change-image-theme-ib">
                    <img src="/transport/img/icons/budget-balance.svg" class="modal-icon" alt="Send Money Icon">
                    <img src="/transport/img/icons/budget-balance-dark-theme.svg" class="modal-icon dark-theme-image" alt="Send Money Icon">
                    <div class="modal-title">
                        ACCOUNT TOP UP
                    </div>
                </div>
                <div class="modal-text">
                    Fund your account and start earning today.
                </div>

                <div class="devider-without-line"></div>

                <!-- Send Money Form Start -->
                <form id="deposit" class="withdraw-form" action="{{route('user.deposit.insert')}}" method="post">
                    @csrf
                     <input type="hidden" name="method_code">
                     <input type="hidden" name="currency">
                    <!-- Form Field Start -->
                    <div class="d-flex align-items-center">
                        <label for="send-money-from">Method</label>
                        <select class="form-select form-control form--control" name="gateway" required>
                                        
                                        @foreach($gatewayCurrency as $data)
                                        <option  value="{{$data->method_code}}" @selected(old('gateway') == $data->method_code) data-gateway="{{ $data }}">{{$data->name}}</option>
                                        @endforeach
                                    </select>
                    </div>
                    <!-- Form Field End --> 
                    
                    
        
                    <!-- Form Field Start -->
                    <div class="d-flex align-items-center">
                        <label for="send-money-amount">Amount</label>
                        <input type="number" step="any" name="amount" placeholder="Enter Amount" class="form-control form--control" value="{{ old('amount') }}" autocomplete="off" required>
                        <span>USD</span>
                    </div>
                    <!-- Form Field End -->
                    
                </form>
                <!-- Send Money Form End -->

            </div>
            <div class="modal-bottom d-flex mt-4">
                <a class="btn btn-light w-100" data-dismiss="modal">CANCEL</a>
                <button onclick="document.getElementById('deposit').submit()" class="btn btn-dark w-100 ml-2">DEPOSIT</button>
            </div>
        </div>
    </div>
</div>
