@extends($activeTemplate.'layouts.masterinner')
@section('content')
<div class="main-content m-100">
      <!-- All Content Start -->
      <div class="content-padding">
 @php($balance = auth()->user()->deposit_wallet)
        <!-- Total Balance Add Money Start -->
        <div class="d-flex justify-content-between align-items-end total-balance-add-money-block">
            <div class="total-balance-block">
                TOTAL BALANCE
                <div class="money-balance">
                    ${{ showAmount(auth()->user()->deposit_wallet) }}
                </div>
            </div>
            <div class="add-money-block text-center change-image-theme">
                <img src="/transport/img/icons/add-money-icon.svg" alt="Add Money Icon">
                <img src="/transport/img/user.png" class="dark-theme-image" alt="Add Money Icon">
                {{ auth()->user()->fullname }}
            </div>
        </div>
        <!-- Total Balance Add Money End -->

       
        
        <!-- Devide Start -->
        <div class="devider right"></div>
        <!-- Devide End -->
        
             
               <!-- Money Loan Block Start -->
        <div class="money-loan-block information-block mt-3">
            
            <div class="money-loan-carousel owl-carousel">
                <div class="item">
                    
                    <div class="graph-info">
                        <div class="graph-title">
                            TOTAL DEPOSITS
                        </div>
                        <div class="information-money">
                            <a href="#">${{ showAmount($successfulDeposits) }}</a>
                        </div>
                        
                    </div>
                </div>
                <div class="item">
                    
                    <div class="graph-info">
                        <div class="graph-title">
                            TOTAL WITHDRAWS
                        </div>
                        <div class="information-money">
                            <a href="#">${{ showAmount($successfulWithdrawals) }}</a>
                        </div>
                        
                    </div>
                </div>
                
            </div>
        </div>
        <!-- Money Loan Block End -->
        <!-- Charts Block Start -->
        <div class="charts-block information-block mt-3">
            <div class="information-block_title d-flex justify-content-between align-items-center">
                <div>
                    ACCOUNTS
                </div>
                <a href="{{ route('user.transactions') }}" class="text-underline">View Transactions</a>
            </div>
           
            <div class="charts-information d-flex">
                <div class="item-information income-information">
                    <div class="information-title d-flex align-items-center">
                        SELECT INCOME
                    </div>
                    <div class="information-money">
                        ${{ showAmount($totalEarningToday) }}
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                   
                </div>
                <div class="item-information expense-information">
                    <div class="information-title d-flex align-items-center">
                        TOTAL INCOME
                    </div>
                    <div class="information-money">
                        ${{ showAmount($interests) }}
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    
                </div>                
            </div>
            <div class="charts-information d-flex">
                <div class="item-information bills-information">
                    <div class="information-title d-flex align-items-center">
                        TOTAL BONUS
                    </div>
                    <div class="information-money">
                        ${{ showAmount($referral_earnings) }}
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                   
                </div>
                <div class="item-information saving-information">
                    <div class="information-title d-flex align-items-center">
                        BONUS TODAY
                    </div>
                    <div class="information-money">
                        ${{ showAmount($referral_earnings_today) }}
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                   
                </div>
            </div>

        </div>
        <!-- Charts Block End -->

        
        <!-- Copyright Block Start -->

@endsection
