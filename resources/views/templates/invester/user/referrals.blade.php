@extends($activeTemplate.'layouts.masterinner')
@section('content')
@php
use App\Models\Deposit;

@endphp
@php
$balance = auth()->user()->deposit_wallet;
@endphp
   <!-- Content Title & Slogan Start -->
    <div class="sub-pages-top-content-section top-box-padding">
        <div class="d-flex align-items-center">
            <div class="title-image mr-2 change-image-theme">
                <img src="/transport/img/icons/components-icon.svg" class="d-block" alt="Reusable Components Icon">
                <img src="/transport/img/icons/components-icon-dark-theme.svg" class="dark-theme-image" alt="Reusable Components Icon">
            </div>
            <div class="title">
                Referral Information
            </div>
        </div>
        <div class="slogan">
            Check your team that helps you move things.
        </div>
        <!-- Devide Start -->
        <div class="devider-without-line mb-2"></div>
        <!-- Devide End -->
    </div>
    <!-- Content Title & Slogan End -->
    <div class="main-content p-100">

      <!-- All Content Start -->
      <div class="content-padding">

 
              
              <!-- Dark Striped Tab Start -->
          <div class="pages-list_title">

              <div class="title">
                  Referrals
              </div>
            
              <table class="table table-striped fw-100">
                 <tbody>
                  @php
         $dep = Deposit::where('user_id', $user->id)->where('status', 1)->sum('amount');
        @endphp

                     @if($user->allReferrals->count() > 0 && $maxLevel > 0)
          
                    <div class="treeview-container">
                        <ul class="treeview">
            <div class="alerts-list mt-5">
              
              <div class="item-list d-flex align-items-center shadow-none br-05 bg-color justify-content-between pl-4 pr-4 fw-100 mt-2">
                  <div class="d-flex ff-2 fw-100">
                      <div class="user-image align-items-center mr-2">
                          <img src="/transport/img/pic.png">
                      </div>
                     {{ $user->fullname }} (You)
                  </div>
                  <style>
  #copyBtn {
    font-size: 12px;
    padding: 4px 8px;
  }
</style>
                  <div class="coupon-form">
  <input class="form-control" type="text" id="myInput" value="{{ route('user.register') }}?reference={{ auth()->user()->username }}" readonly style="display: none;">
  <button class="btn btn-primary btn-sm" id="copyBtn">Copy Your Link</button>
</div>

<script>
  var copyBtn = document.getElementById("copyBtn");
  var input = document.getElementById("myInput");

  copyBtn.addEventListener("click", function() {
    input.style.display = "block";
    input.select();
    document.execCommand("copy");
    input.style.display = "none";
    alert("Copied the referral link to clipboard!");
  });
</script>
                  
              </div>
              @include($activeTemplate.'partials.under_tree',['user'=>$user,'layer'=>0,'isFirst'=>true])
          </div>
                       
             
            </div>
            @endif


                </tbody>
              </table>


          </div>
          <!-- Dark Striped Tab End -->

        
      </div>
      <!-- All Content End -->
    </div>

@endsection
@push('style')
    <link href="{{ asset('assets/global/css/jquery.treeView.css') }}" rel="stylesheet" type="text/css">
@endpush
@push('script')
<script src="{{ asset('assets/global/js/jquery.treeView.js') }}"></script>
 <script>
   function myFunction() {
  // Get the text field
  var copyText = document.getElementById("myInput");

  // Select the text field
  copyText.select();
  copyText.setSelectionRange(0, 99999); // For mobile devices

   // Copy the text inside the text field
  navigator.clipboard.writeText(copyText.value);

  // Alert the copied text
  alert("Copied the text: " + copyText.value);
}
</script>
@endpush