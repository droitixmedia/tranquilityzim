@extends($activeTemplate . 'layouts.masterinner')
@section('content')
<!-- Content Title & Slogan Start -->
    <div class="sub-pages-top-content-section top-box-padding">
        <div class="d-flex align-items-center">
            <div class="title-image mr-2 change-image-theme">
                <img src="/transport/img/icons/transactions.svg" class="d-block" alt="Paperwork Icon">
                <img src="/transport/img/icons/transactions-dark-theme.svg" class="dark-theme-image" alt="Paperwork Icon">
            </div>
            <div class="title">
                Deposit Transactions
            </div>
        </div>
        <div class="slogan">
            A history of Done and Pending Top Up Transactions.
        </div>
        <!-- Devide Start -->
        <div class="devider-without-line mb-2"></div>
        <!-- Devide End -->
    </div>
    <div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">


          <!-- Transactions Block Start -->
          <div class="transactions-block information-block mt-110">
              
              @forelse($deposits as $deposit)
              @if ($deposit->status == 2)
              <a href="#" class="item-transaction d-flex align-items-center">
                  <div class="item-transaction_transaction-image d-flex justify-content-center align-items-center">
                    @if($deposit->gateway?->name =='ECOCASH USD')
                      <img width="40" src="/transport/img/dollar.svg" class="border-50" alt="Transaction Image">
                     @else
                     <img width="40" src="/transport/img/dollar.svg" class="border-50" alt="Transaction Image">
                     @endif 
                  </div>
                  <div class="item-transaction_transaction-title">
                      {{ __($deposit->gateway?->name) }} DEPOSIT
                      <div class="transaction-type">
                          ..Pending
                      </div>
                      <div class="transaction-type">
                          {{ showDateTime($deposit->created_at) }}
                      </div>
                  </div>
                  <div class="item-transaction_transaction-amount ml-auto positive">
                      +{{ showAmount($deposit->amount) }} {{ $general->cur_text }}
                  </div>
                  <div class="item-transaction_transaction-amount ml-auto positive">
                    <form action="/user/deposits/{{ $deposit->id }}" method="POST">
    @method('DELETE')
    @csrf
     <button style="background: transparent; border:none;" onclick="document.querySelector('form').submit();">
  <img width="20" src="/transport/img/delete.svg">
</button>
</form>
                     
                  </div>
              </a>
              @elseif($deposit->status == 1)
               <a href="#" class="item-transaction d-flex align-items-center">
                  <div class="item-transaction_transaction-image d-flex justify-content-center align-items-center">
                   
                      <img width="40" src="/transport/img/icons/approved-dark-theme.svg" class="border-50" alt="Transaction Image">
                 
                  </div>
                  <div class="item-transaction_transaction-title">
                      {{ __($deposit->gateway?->name) }} DEPOSIT
                      <div class="transaction-type">
                          Complete
                      </div>
                      <div class="transaction-type">
                          {{ showDateTime($deposit->created_at) }}
                      </div>
                  </div>
                  <div style="color: green;" class="item-transaction_transaction-amount ml-auto">
                      +{{ showAmount($deposit->amount) }} {{ $general->cur_text }}
                  </div>
                  
              </a>
              @elseif($deposit->status == 3)
              <a href="#" class="item-transaction d-flex align-items-center">
                  <div class="item-transaction_transaction-image d-flex justify-content-center align-items-center">
                   
                      <img width="40" src="/transport/img/icons/rejected-dark-theme.svg" class="border-50" alt="Transaction Image">
                 
                  </div>
                  <div class="item-transaction_transaction-title">
                      {{ __($deposit->gateway?->name) }} DEPOSIT
                      <div class="transaction-type">
                          Failed
                      </div>
                      <div class="transaction-type">
                          We did not see funds
                      </div>
                  </div>
                  <div style="color: red;" class="item-transaction_transaction-amount ml-auto">
                      {{ showAmount($deposit->amount) }} {{ $general->cur_text }}
                  </div>
                  
              </a>
              @else

              @endif
              @empty

              <div   style="display: flex; justify-content: center; align-items: center;">
                      <img width="70" style="align-content: center;" src="/transport/img/icons/rejected.svg" class="border-50" alt="Transaction Image"><br>

              </div>
              @endforelse
              
            
          </div>
        
      </div>
      <!-- All Content End -->
    </div>
    <script>
  document.querySelector('form').addEventListener('submit', function (event) {
    event.preventDefault();

    fetch(this.action, {
      method: this.method,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-CSRF-TOKEN': document.querySelector('input[name="_token"]').value
      }
    })
    .then(response => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }

      window.location.replace('/');
    })
    .catch(error => {
      console.error(error);
    });
  });
</script>
@endsection


@push('script')

    <script>
        (function($) {
            "use strict";
            $('.detailBtn').on('click', function() {
                var modal = $('#detailModal');

                var userData = $(this).data('info');
                var html = '';
                if (userData) {
                    userData.forEach(element => {
                        if (element.type != 'file') {
                            html += `
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                <span>${element.name}</span>
                                <span">${element.value}</span>
                            </li>`;
                        }
                    });
                }

                modal.find('.userData').html(html);

                if ($(this).data('admin_feedback') != undefined) {
                    var adminFeedback = `
                        <div class="my-3">
                            <strong>@lang('Admin Feedback')</strong>
                            <p>${$(this).data('admin_feedback')}</p>
                        </div>
                    `;
                } else {
                    var adminFeedback = '';
                }

                modal.find('.feedback').html(adminFeedback);


                modal.modal('show');
            });
        })(jQuery);
    </script>
@endpush
