@extends($activeTemplate.'layouts.masterinner')
@section('content')

      <div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">

        <!-- Content Title & Slogan Start -->
        <div class="sub-pages-top-content-section">
            <div class="d-flex align-items-center">
                <div class="title-image mr-2 change-image-theme">
                    <img src="/transport/img/icons/my-cards.svg" class="d-block" alt="Paperwork Icon">
                    <img src="/transport/img/icons/my-cards-dark-theme.svg" class="dark-theme-image" alt="Paperwork Icon">
                </div>
                <div class="title">
                    Edit Profile
                </div>
            </div>
            <div class="slogan">
                Configure your profile to your requirements.
            </div>
            <!--<div class="add-button mt-3 mb-3">

                <div class="title">
                    + Add a new card

                </div>

            </div>-->
        </div>
        <!-- Content Title & Slogan End -->

        <!-- Devide Start -->
        <div class="devider-without-line"></div>
        <!-- Devide End -->
      <form action="" method="POST">
                @csrf
      <div class="card-form">
          <label>Username</label>
          <input class="form-control form-control-lg" type="text" name="username" value="{{$user->username}}" aria-label=".form-control-lg example">

         
            <div class="card-name">
                <label>Phone Number</label>
                <input class="form-control" type="text" name="mobile"  value="{{$user->mobile}}" aria-label=".form-control-lg example">
            </div>
            <div class="card-name">
                <label>First Name</label>
                <input class="form-control" type="text" name="firstname"  value="{{$user->firstname}}" aria-label=".form-control-lg example">
            </div>
            <div class="card-name">
                <label>Last Name</label>
                <input class="form-control" type="text" name="lastname"  value="{{$user->lastname}}" aria-label=".form-control-lg example">
            </div>
            <div class="card-name">
                <label>Email Address</label>
                <input class="form-control" type="text" name="email"  value="{{ $user->email }}" aria-label=".form-control-lg example" readonly>
            </div>
            <input type="hidden" name="address" value="Zambia">
                <input type="hidden" name="state" value="Zambia">
                <input type="hidden" name="zip" value="411320">
                <input type="hidden" name="city" value="Lusaka">
                <input type="hidden" name="country" value="Zambia">

            <button type="submit"  class="btn btn-primary">SAVE DATA</button>

      </div>
    </form>

              <!-- Form Field End -->




          <!-- Transactions Block End -->
        



        
      </div>
      <!-- All Content End -->
    </div>

@endsection
