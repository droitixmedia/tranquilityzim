@extends($activeTemplate . 'layouts.masterinner')
@section('content')
 <div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">

        <!-- Content Title & Slogan Start -->
        <div class="sub-pages-top-content-section">
            <div class="d-flex align-items-center">
                <div class="title-image mr-2 change-image-theme">
                    <img src="/transport/img/icons/my-cards.svg" class="d-block" alt="Paperwork Icon">
                    <img src="/transport/img/icons/sms-verification.svg" class="dark-theme-image" alt="Paperwork Icon">
                </div>
                <div class="title">
                    Change Password
                </div>
            </div>
            <div class="slogan">
                Configure your profile to your requirements.
            </div>
            <!--<div class="add-button mt-3 mb-3">

                <div class="title">
                    + Add a new card

                </div>

            </div>-->
        </div>
        <!-- Content Title & Slogan End -->

        <!-- Devide Start -->
        <div class="devider-without-line"></div>
        <!-- Devide End -->
      <form action="" method="POST">
                @csrf
      <div class="card-form">
          <label>Current Password</label>
          <input class="form-control form-control-lg" type="password" name="current_password" required autocomplete="current-password" aria-label=".form-control-lg example">

         
            <div class="card-name">
                <label>New Password</label>
                <input class="input-psswd form-control" type="password" name="password" required autocomplete="current-password" aria-label=".form-control-lg example">
            </div>
            <div class="card-name">
                <label>Confirm New Password</label>
                <input class="form-control" type="password" name="password_confirmation" required autocomplete="current-password" aria-label=".form-control-lg example">
            </div>
            

            <button type="submit"  class="btn btn-primary">CHANGE PASSWORD

      </div>
    </form>

              <!-- Form Field End -->




          <!-- Transactions Block End -->
        



        
      </div>
      <!-- All Content End -->
    </div>
@endsection

@if ($general->secure_password)
    @push('script-lib')
        <script src="{{ asset('assets/global/js/secure_password.js') }}"></script>
    @endpush
@endif
