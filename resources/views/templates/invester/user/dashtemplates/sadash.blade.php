 <div class="page-content-wrapper">
      <!-- Search Form-->
      <!-- Search Form-->
      <div class="container">
        <div class="search-form pt-4 rtl-flex-d-row-r">
          <form action="#" method="">


            <input class="form-control" type="search" placeholder="Welcome {{ auth()->user()->firstname }} " disabled="true">
            <button type="submit"><i class="fa-solid fa-user"></i></button>
          </form>



          <!-- Alternative Search Options -->
          <div class="alternative-search-options">
            <div class="dropdown"><a class="btn btn-success dropdown-toggle" id="altSearchOption" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa-solid fa-sliders"></i></a>
              <!-- Dropdown Menu -->
              <ul class="dropdown-menu dropdown-menu-end" aria-labelledby="altSearchOption">
                <li><a class="dropdown-item" href="#"><i class="fa-solid fa-microphone"> </i>Voice Search</a></li>
                <li><a class="dropdown-item" href="#"><i class="fa-solid fa-image"> </i>Image Search</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <!-- Checkout Wrapper-->
        <div class="checkout-wrapper-area py-3">
      <!-- Cart Amount Area-->
          <div class="card cart-amount-area">
            <div class="card-body d-flex align-items-center justify-content-between">
              <h5 class="total-price mb-0">R<span class="counter">{{ showAmount(auth()->user()->deposit_wallet) }}</span></h5><a class="btn btn-warning" href="https://play.google.com/store/apps/details?id=com.door_step.pro"><i class="fa-brands fa-google-play"></i> App</a><a class="btn btn-primary" href="{{ url('/support') }}"><i class="fa-solid fa-headset lni-tada-effect"></i> Support</a>
            </div>
          </div>
      </div>
  </div>

      <!-- Hero Wrapper -->
      <div class="hero-wrapper">
        <div class="container">
          <div class="pt-3">
            <!-- Hero Slides-->
            <div class="hero-slides owl-carousel">
              <!-- Single Hero Slide-->
              <div class="single-hero-slide" style="background-image: url('/img/bg-img/1.jpg')">
                <div class="slide-content h-100 d-flex align-items-center">
                  <div class="slide-text">
                  <a class="btn btn-warning" href="" data-animation="fadeInUp" data-delay="800ms" data-duration="1000ms"><i class="fa-brands fa-whatsapp"></i> Join Our Whatsapp Group</a><br>
                  <a class="btn btn-primary" href="
" data-animation="fadeInUp" data-delay="800ms" data-duration="1000ms"><i class="fa-brands fa-telegram"></i> Join Our Telegram</a>
                  </div>
                </div>
              </div>
              <!-- Single Hero Slide-->
              <div class="single-hero-slide" style="background-image: url('/img/bg-img/2.jpg')">
                <div class="slide-content h-100 d-flex align-items-center">
                  <div class="slide-text">

                  </div>
                </div>
              </div>
              <!-- Single Hero Slide-->
              <div class="single-hero-slide" style="background-image: url('/img/bg-img/3.jpg')">
                <div class="slide-content h-100 d-flex align-items-center">
                  <div class="slide-text">

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Product Catagories -->
      <div class="product-catagories-wrapper py-3">
        <div class="container">
          <div class="row g-2 rtl-flex-d-row-r">

            <!-- Catagory Card -->
            <div class="col-4">
             @if ($balance >= 100 && $balance < 1000)
              <div  class="card catagory-card">
                <a href="{{ route('level1') }}">
                <!-- Badge--><span class="badge badge-success custom-badge"><i class="fa-solid fa-lock-open"><h6>R100 - 500</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/1.png"  alt=""><span></span></div>
                <h6>5% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>
              @else
                 <div  class="card catagory-card">
                <a href="{{ route('user.deposit.index') }}">
                <!-- Badge--><span class="badge badge-warning custom-badge"><i class="fa-solid fa-lock"><h6>R100 - 500</h6></i></span>
                <div class="card-body px-4"><img width="100" height="100" src="/img/core-img/1.png"  alt=""><span></span></div>
                <h6>5% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" color="red"></div>
                  </div>
                  </a>
              </div>

              @endif
            </div>
             <!-- Catagory Card -->
            <div class="col-4">
             @if ($balance >= 1000 && $balance < 6000)
              <div  class="card catagory-card">
                <a href="{{ route('level2') }}">
                <!-- Badge--><span class="badge badge-success custom-badge"><i class="fa-solid fa-lock-open"><h6>R1000 - 5000</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/2.png"  alt=""><span></span></div>
                <h6>6% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>
              @else
                 <div  class="card catagory-card">
                <a href="{{ route('user.deposit.index') }}">
                <!-- Badge--><span class="badge badge-warning custom-badge"><i class="fa-solid fa-lock"><h6>R1000 - 5000</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/2.png"  alt=""><span></span></div>
                <h6>6% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>

              @endif
            </div>
            <!-- Catagory Card -->
            <div class="col-4">
                           @if ($balance >= 6000 && $balance < 11000)
              <div  class="card catagory-card">
                <a href="{{ route('level3') }}">
                <!-- Badge--><span class="badge badge-success custom-badge"><i class="fa-solid fa-lock-open"><h6>R6000 - 10000</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/3.png"  alt=""><span></span></div>
                <h6>7% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>
              @else
                 <div  class="card catagory-card">
                <a href="{{ route('user.deposit.index') }}">
                <!-- Badge--><span class="badge badge-warning custom-badge"><i class="fa-solid fa-lock"><h6>R6000 - 10000</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/3.png"  alt=""><span></span></div>
                <h6>7% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>

              @endif
            </div>
            <!-- Catagory Card -->
            <div class="col-4">
                           @if ($balance >= 11000 && $balance < 101000)
              <div  class="card catagory-card">
                <a href="{{ route('level4') }}">
                <!-- Badge--><span class="badge badge-success custom-badge"><i class="fa-solid fa-lock-open"><h6>R11000 - 100000</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/4.png"  alt=""><span></span></div>
                <h6>10% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>
              @else
                 <div  class="card catagory-card">
                <a href="{{ route('user.deposit.index') }}">
                <!-- Badge--><span class="badge badge-warning custom-badge"><i class="fa-solid fa-lock"><h6>R11000 - 100000</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/4.png"  alt=""><span></span></div>
                <h6>10% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>

              @endif
            </div>
            <!-- Catagory Card -->
            <div class="col-4">
                           @if ($balance >= 101000 && $balance < 510000)
              <div  class="card catagory-card">
                <a href="{{ route('level5') }}">
                <!-- Badge--><span class="badge badge-success custom-badge"><i class="fa-solid fa-lock-open"><h6>R101000 - 500000</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/5.png"  alt=""><span></span></div>
                <h6>12% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>
              @else
                 <div  class="card catagory-card">
                <a href="{{ route('user.deposit.index') }}">
                <!-- Badge--><span class="badge badge-warning custom-badge"><i class="fa-solid fa-lock"><h6>R101000 - 500000</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/5.png"  alt=""><span></span></div>
                <h6>12% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>

              @endif
            </div>
            <!-- Catagory Card -->
            <div class="col-4">
                           @if ($balance >= 510000)
              <div  class="card catagory-card">
                <a href="{{ route('level6') }}">
                <!-- Badge--><span class="badge badge-success custom-badge"><i class="fa-solid fa-lock-open"><h6>R510000 - 1M</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/6.png"  alt=""><span></span></div>
                <h6>14% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>
              @else
                 <div  class="card catagory-card">
                <a href="{{ route('user.deposit.index') }}">
                <!-- Badge--><span class="badge badge-warning custom-badge"><i class="fa-solid fa-lock"><h6>R510000 - 1M</h6></i></span>
                <div class="card-body px-2"><img width="100" height="100" src="/img/core-img/6.png"  alt=""><span></span></div>
                <h6>14% per Day</h6>
                </p>
                  <!-- Progress Bar-->
                  <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                  </div>
                  </a>
              </div>

              @endif
            </div>

          </div>
        </div>
      </div>
      <!-- Flash Sale Slide -->
      <div class="flash-sale-wrapper">
        <div class="container">
          <div class="section-heading d-flex align-items-center justify-content-between rtl-flex-d-row-r">
            <h6 class="d-flex align-items-center rtl-flex-d-row-r"><i class="fa-solid fa-bolt-lightning me-1 text-success lni-flashing-effect"></i>Live System Activity</h6>
            <!-- Please use event time this format: YYYY/MM/DD hh:mm:ss -->
            <ul class="sales-end-timer ps-0 d-flex align-items-center rtl-flex-d-row-r">
             <i class="fa-solid fa-clock"></i>&nbsp&nbsp<time id="currentTime"></time>
            </ul>
          </div>
          <!-- Flash Sale Slide-->
          <div class="flash-sale-slide owl-carousel">

          </div>
        </div>
      </div><br>
      <!-- Dark Mode -->
      <div class="container">
        <div class="discount-coupon-card p-4 p-lg-5 dir-rtl">
          <div class="d-flex align-items-center">
            <div class="discountIcon"><img class="w-100" src="/img/core-img/price-tag.png" alt=""></div>
            <div class="text-content">
              <h4 class="text-white mb-1">Get 10% Bonus!</h4>
              <p class="text-white mb-0">Every time your downliner<span class="px-1 fw-bold">DEPOSITS</span>you get 10% percent.</p>
            </div>
          </div>
        </div>
      </div><br>

      <div class="pb-3">
        <div class="container">
          <div class="section-heading d-flex align-items-center justify-content-between dir-rtl">
            <h6>Weekly Winners</h6><a class="btn p-0" href="#">
               With Prizes</a>
          </div>
          <!-- Collection Slide-->
          <div class="collection-slide owl-carousel">
            <!-- Collection Card-->
            <div class="card collection-card"><a href="#"><img src="/img/product/17.png" alt=""></a>
              <div class="collection-title"><span>TBD</span><span class="badge bg-success"><i class="fa-solid fa-trophy"></i> R500</span></div>
            </div>
            <!-- Collection Card-->
            <div class="card collection-card"><a href="single-product.html"><img src="/img/product/20.png" alt=""></a>
              <div class="collection-title"><span>TBD</span><span class="badge bg-warning"><i class="fa-solid fa-trophy"></i> R400</span></div>
            </div>
            <!-- Collection Card-->
            <div class="card collection-card"><a href="single-product.html"><img src="/img/product/18.png" alt=""></a>
              <div class="collection-title"><span>TBD</span><span class="badge bg-primary"><i class="fa-solid fa-trophy"></i> R350</span></div>
            </div>
            <!-- Collection Card-->
            <div class="card collection-card"><a href="single-product.html"><img src="/img/product/21.png" alt=""></a>
              <div class="collection-title"><span>TBD</span><span class="badge bg-danger"><i class="fa-solid fa-trophy"></i> R350</span></div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- Internet Connection Status-->
    <div class="internet-connection-status" id="internetStatus"></div>
