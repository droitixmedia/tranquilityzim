@extends($activeTemplate . 'layouts.appnew')
@section('panel')
    @php
        $authContent = getContent('authentication.content', true);
    @endphp
  <!-- Header Start -->
  <div class="main-header w-100">
    <div class="top-row d-flex justify-content-between align-items-center">
        <div class="time">
            <img src="/transport/img/icons/i-phone-x-overrides-time-black.svg" alt="">
            <img src="/transport/img/icons/time-dark-theme.svg" class="dark-theme-image" alt="">

        </div>
        <div class="mobile-info-icons">
            <img src="/transport/img/icons/phone-icons.svg" alt="phone-icons">
            <img src="/transport/img/icons/phone-icons-white.svg" class="dark-theme-image" alt="phone-icons">

        </div>
    </div>
  </div>
  <!-- Header End -->

    <div class="main-content h-100">
      <!-- All Content Start -->
      <div class="content-padding h-100 d-flex flex-column justify-content-center min-600">

        <!-- Logo FinHit Start -->
        <div class="text-center d-flexjustify-content-center align-items-center mt-auto change-image-theme-ib">
          <img src="/transport/img/big_logo.svg" alt="FinHit Logo">
          <img src="/transport/img/big_logo-dark-theme.png" class="dark-theme-image" alt="FinHit Logo">

          <!-- Slogan Start -->
          <div class="slogan text-center">Certe, inquam, pertinax non intellegamus, tu paulo ante cum memoriter, tum etiam erga nos causae.</div>
          <!-- Slogan End -->

        </div>
        <!-- Logo FinHit End -->

        <!-- Sign Up Form Start -->
        <form class="sing-up-form">
            <!-- Form Field Start -->
            <div>
                <input class="form-control" type="text" autocomplete="off" name="name_last_name" placeholder="Name, Lastname">
            </div>
            <!-- Form Field End -->

            <!-- Form Field Start -->
            <div>
                <input class="form-control" type="email" autocomplete="off" name="email" placeholder="E-mail">
            </div>
            <!-- Form Field End -->             

            <!-- Form Field Start -->
            <div>
                <input class="form-control" type="password" autocomplete="off" name="password" placeholder="Password">
            </div>
            <!-- Form Field End -->

            <!-- Form Field Start -->
            <div>
                <input class="form-control" type="password" autocomplete="off" name="confirm_password" placeholder="Confirm Password">
            </div>
            <!-- Form Field End -->
        </form>
        <!-- Sign Up Form End -->

        <!-- Registration Row Start -->
        <div class="d-flex justify-content-between align-items-center w-100 mt-2 registration-row">
            <div>
                Have account? <a href="sign-in.html" class="text-underline">Sign In</a>
            </div>
            <i><a href="forgot-password.html">Forgot Password?</a></i>
        </div>
        <!-- Registration Row End -->

        <!-- Sign In Button Start -->
        <a href="sms-verification.html" class="btn btn-light">Sign Up</a>
        <!-- Sign In Button End -->

        <!-- Copyright Block Start -->
        <div class="copyright-block text-center mt-auto">
          Copyright &copy; <span>Fin</span><span>Hit</span> 2021. All Rights Reserved.
        </div>
        <!-- Copyright Block End -->
        
      </div>
      <!-- All Content End -->
    </div>
@endsection

@if ($general->secure_password)
    @push('script-lib')
        <script src="{{ asset('assets/global/js/secure_password.js') }}"></script>
    @endpush
@endif

@push('script')
    <script>
        "use strict";
        (function($) {
            @if ($mobileCode)
                $(`option[data-code={{ $mobileCode }}]`).attr('selected', '');
            @endif
            $('select[name=country]').change(function() {
                $('input[name=mobile_code]').val($('select[name=country] :selected').data('mobile_code'));
                $('input[name=country_code]').val($('select[name=country] :selected').data('code'));
                $('.mobile-code').text('+' + $('select[name=country] :selected').data('mobile_code'));
            });
            $('input[name=mobile_code]').val($('select[name=country] :selected').data('mobile_code'));
            $('input[name=country_code]').val($('select[name=country] :selected').data('code'));
            $('.mobile-code').text('+' + $('select[name=country] :selected').data('mobile_code'));
            $('.checkUser').on('focusout', function(e) {
                var url = '{{ route('user.checkUser') }}';
                var value = $(this).val();
                var token = '{{ csrf_token() }}';
                if ($(this).attr('name') == 'mobile') {
                    var mobile = `${$('.mobile-code').text().substr(1)}${value}`;
                    var data = {
                        mobile: mobile,
                        _token: token
                    }
                }
                if ($(this).attr('name') == 'email') {
                    var data = {
                        email: value,
                        _token: token
                    }
                }
                if ($(this).attr('name') == 'username') {
                    var data = {
                        username: value,
                        _token: token
                    }
                }
                $.post(url, data, function(response) {
                    if (response.data != false && response.type == 'email') {
                        $('#existModalCenter').modal('show');
                    } else if (response.data != false) {
                        $(`.${response.type}Exist`).text(`${response.type} already exist`);
                    } else {
                        $(`.${response.type}Exist`).text('');
                    }
                });
            });
        })(jQuery);
    </script>
@endpush
