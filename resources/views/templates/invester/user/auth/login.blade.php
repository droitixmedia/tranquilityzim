@extends($activeTemplate . 'layouts.appauth')
@section('panel')
    @php
        $authContent = getContent('authentication.content', true);
    @endphp
  

    <div class="main-content h-100">
      <!-- All Content Start -->
      <div class="content-padding h-100 d-flex flex-column justify-content-center min-600">

        <!-- Logo FinHit Start -->
        <div class="text-center d-flexjustify-content-center align-items-center mt-auto change-image-theme-ib">
          <img src="/transport/img/big_logo.svg" alt="FinHit Logo">
          <img src="/transport/img/logo.png" class="dark-theme-image" alt="FinHit Logo">

          <!-- Slogan Start -->
          <div class="slogan text-center">Login</div>
          <!-- Slogan End -->

        </div>
        <!-- Logo FinHit End -->

        <!-- Sign Up Form Start -->
        <form id="reg" class="sing-up-form" action="{{ route('user.login') }}" method="POST">
            @csrf
 
          
           
            <div>
                <input class="form-control" name="username" id="username" type="text" placeholder="Enter Email or Username">
            </div>
                  
        
            <div>
                <input class="input-psswd form-control" id="registerPassword" type="password" name="password" placeholder="Enter Password">
            </div>
           

            

      

        </form>
        <!-- Sign Up Form End -->

        <!-- Registration Row Start -->
        <div class="d-flex justify-content-between align-items-center w-100 mt-2 registration-row">
            <div>
                Dont Have Account? <a href="{{ route('user.register') }}" class="text-underline">Register</a>
            </div>
            <i><a href="{{ route('user.password.request') }}">Forgot Password?</a></i>
        </div>
        <!-- Registration Row End -->
        
        
        <button onclick="document.getElementById('reg').submit()" class="btn btn-light">Login</button>

    
        
    
        <!-- Copyright Block Start -->
        <div class="copyright-block text-center mt-auto">
          Copyright &copy; <span>myTransport</span><span></span> 2023. All Rights Reserved.
        </div>
        <!-- Copyright Block End -->
        
      </div>
      <!-- All Content End -->
    </div>
@endsection
