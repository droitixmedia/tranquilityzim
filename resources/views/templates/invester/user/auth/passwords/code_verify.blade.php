@extends($activeTemplate.'layouts.appauth')
@section('panel')

  <div class="main-content h-100">
      <!-- All Content Start -->
      <div class="content-padding h-100 d-flex flex-column justify-content-center min-600">

        <!-- Logo FinHit Start -->
        <div class="text-center d-flexjustify-content-center align-items-center mt-auto change-image-theme-ib">
          <img src="/transport/img/icons/forgot-password.svg" alt="FinHit Logo">
          <img src="/transport/img/logo.png" class="dark-theme-image" alt="FinHit Logo">
            <div class="title forgot-password-title ff-2 fw-300 fs-18 mt-3">
                VERIFICATION
            </div>
          <!-- Slogan Start -->
          <div class="slogan text-center">Enter code sent to {{ showEmailAddress($email) }}.</div>
          <!-- Slogan End -->

        </div>
        <!-- Logo FinHit End -->

        <!-- Sign Up Form Start -->
        <form id="code" class="sing-up-form" action="{{ route('user.password.verify.code') }}" method="POST">

           @csrf
               <input type="hidden" name="email" value="{{ $email }}">

                        @include($activeTemplate.'partials.verification_code')
       </form>
        <div class="d-flex justify-content-between align-items-center w-100 mt-2 registration-row">
          <a href="{{ route('user.password.request') }}" class="text-underline">Resend Code Again</a>
         
        </div>

        <button onclick="document.getElementById('code').submit()" class="btn btn-light">Verify</button>

        <!-- Copyright Block Start -->
        <div class="copyright-block text-center mt-auto">
          Copyright &copy; <span>myTransport</span><span></span> 2023. All Rights Reserved.
        </div>
        <!-- Copyright Block End -->
        
      </div>
      <!-- All Content End -->
    </div>

@endsection
@push('script')
    <script>
        $('#verification-code').on('input', function () {
            $(this).val(function(i, val){
                if (val.length >= 6) {
                    $('.submit-form').find('button[type=submit]').html('<i class="las la-spinner fa-spin"></i>');
                    $('.submit-form').submit()
                }
                if(val.length > 6){
                    return val.substring(0, val.length - 1);
                }
                return val;
            });
            for (let index = $(this).val().length; index >= 0 ; index--) {
                $($('.boxes span')[index]).html('');
            }
        });
    </script>
@endpush