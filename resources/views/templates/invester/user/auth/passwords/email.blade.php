@extends($activeTemplate.'layouts.appauth')
@section('panel')

  <div class="main-content h-100">
      <!-- All Content Start -->
      <div class="content-padding h-100 d-flex flex-column justify-content-center min-600">

        <!-- Logo FinHit Start -->
        <div class="text-center d-flexjustify-content-center align-items-center mt-auto change-image-theme-ib">
          <img src="/transport/img/icons/forgot-password.svg" alt="FinHit Logo">
          <img src="/transport/img/logo.png" class="dark-theme-image" alt="FinHit Logo">
            <div class="title forgot-password-title ff-2 fw-300 fs-18 mt-3">
                FORGOT PASSWORD?
            </div>
          <!-- Slogan Start -->
          <div class="slogan text-center">We will send a reset link to your email.</div>
          <!-- Slogan End -->

        </div>
        <!-- Logo FinHit End -->

        <!-- Sign Up Form Start -->
        <form id="reset" class="sing-up-form" action="{{ route('user.password.email') }}" method="POST">

          @csrf
          <!-- Form Field Start -->
          <div>
              <input class="form-control" type="text" name="value" value="{{ old('value') }}"  autofocus="off" placeholder="Enter your email">
          </div>
          <!-- Form Field End -->          

        </form>
        <!-- Sign Up Form End -->

        <!-- Registration Row Start -->
        <div class="d-flex justify-content-between align-items-center w-100 mt-2 registration-row">
          <a href="{{ route('user.login') }}" class="text-underline">You remember your password? login</a>
         
        </div>
        <!-- Registration Row End -->

        <button onclick="document.getElementById('reset').submit()" class="btn btn-light">Send Reset Link</button>

        <!-- Copyright Block Start -->
        <div class="copyright-block text-center mt-auto">
          Copyright &copy; <span>myTransport</span><span></span> 2023. All Rights Reserved.
        </div>
        <!-- Copyright Block End -->
        
      </div>
      <!-- All Content End -->
    </div>

@endsection
