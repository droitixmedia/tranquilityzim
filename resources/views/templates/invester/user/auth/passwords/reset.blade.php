@extends($activeTemplate.'layouts.appauth')
@section('panel')
<div class="main-content h-100">
      <!-- All Content Start -->
      <div class="content-padding h-100 d-flex flex-column justify-content-center min-600">

        <!-- Logo FinHit Start -->
        <div class="text-center d-flexjustify-content-center align-items-center mt-auto change-image-theme-ib">
          <img src="/transport/img/icons/forgot-password.svg" alt="FinHit Logo">
          <img src="/transport/img/logo.png" class="dark-theme-image" alt="FinHit Logo">
            <div class="title forgot-password-title ff-2 fw-300 fs-18 mt-3">
                CHANGE PASSWORD?
            </div>
          <!-- Slogan Start -->
          <div class="slogan text-center">Lets change your password.</div>
          <!-- Slogan End -->

        </div>
        <!-- Logo FinHit End -->

        <!-- Sign Up Form Start -->
        <form id="change" class="sing-up-form" action="{{ route('user.password.update') }}" method="POST">
          @csrf
           <input type="hidden" name="email" value="{{ $email }}">
                  <input type="hidden" name="token" value="{{ $token }}">
          <div>
              <input class="form-control" type="text" autocomplete="off" name="password" placeholder="New Password">
          </div>
                

         
          <div>
              <input class="form-control" type="password" autocomplete="off" name="password_confirmation" placeholder="Confirm Password">
          </div>
    
        </form>
        <!-- Sign Up Form End -->

        <!-- Registration Row Start -->
        <div class="d-flex justify-content-between align-items-center w-100 mt-2 registration-row">
         
        </div>
        <!-- Registration Row End -->

          <button onclick="document.getElementById('change').submit()" class="btn btn-light">Submit</button>

        <!-- Copyright Block Start -->
        <div class="copyright-block text-center mt-auto">
          Copyright &copy; <span>myTransport</span><span></span> 2023. All Rights Reserved.
        </div>
        <!-- Copyright Block End -->
        
      </div>
      <!-- All Content End -->
    </div>
@endsection
