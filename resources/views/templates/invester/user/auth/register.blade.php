@extends($activeTemplate . 'layouts.appauth')
@section('panel')
    @php
        $authContent = getContent('authentication.content', true);
    @endphp
  

    <div class="main-content h-100">
      <!-- All Content Start -->
      <div class="content-padding h-100 d-flex flex-column justify-content-center min-600">

        <!-- Logo FinHit Start -->
        <div class="text-center d-flexjustify-content-center align-items-center mt-auto change-image-theme-ib">
          <img src="/transport/img/big_logo.svg" alt="FinHit Logo">
          <img src="/transport/img/logo.png" class="dark-theme-image" alt="FinHit Logo">

          <!-- Slogan Start -->
          <div class="slogan text-center">Earning Starts Here</div>
          <!-- Slogan End -->

        </div>
        <!-- Logo FinHit End -->

        <!-- Sign Up Form Start -->
        <form id="reg" class="sing-up-form" action="{{ route('user.register') }}" method="POST">
            @csrf
 
            <div>
                <input class="form-control" type="text" autocomplete="off" id="username" name="username" value="{{ old('username') }}" placeholder="Username">
            </div>
           
            <div>
                <input class="form-control checkUser" type="email" autocomplete="off" name="email" placeholder="Email" value="{{ old('email') }}">
            </div>
                  
        
            <div>
                <input class="input-psswd form-control" id="registerPassword" type="password" name="password" placeholder="Enter Password">
            </div>
           

            <div>
                <input class="input-psswd form-control" id="registerPassword" type="password" name="password_confirmation" placeholder="Repeat Password">
            </div>
            <input type="hidden" name="refer_by" value="{{ $reference }}">
                <input type="hidden" name="country" value="Zambia">
                <input type="hidden" name="agree" value="1">
                 <input type="hidden" name="mobile_code" value="+263">
                 <input type="hidden" name="country_code" value="ZM">
 

      

        </form>
        <!-- Sign Up Form End -->

        <!-- Registration Row Start -->
        <div class="d-flex justify-content-between align-items-center w-100 mt-2 registration-row">
            <div>
                Have account? <a href="{{ route('user.login') }}" class="text-underline">Sign In</a>
            </div>
            <i><a href="{{ route('user.password.request') }}">Forgot Password?</a></i>
        </div>
        <!-- Registration Row End -->
        
        
        <button onclick="document.getElementById('reg').submit()" class="btn btn-light">Sign Up</button>

    
        
    
        <!-- Copyright Block Start -->
        <div class="copyright-block text-center mt-auto">
          Copyright &copy; <span>myTransport</span><span></span> 2023. All Rights Reserved.
        </div>
        <!-- Copyright Block End -->
        
      </div>
      <!-- All Content End -->
    </div>
@endsection

@if ($general->secure_password)
    @push('script-lib')
        <script src="{{ asset('assets/global/js/secure_password.js') }}"></script>
    @endpush
@endif

@push('script')
    <script>
        "use strict";
        (function($) {
            @if ($mobileCode)
                $(`option[data-code={{ $mobileCode }}]`).attr('selected', '');
            @endif
            $('select[name=country]').change(function() {
                $('input[name=mobile_code]').val($('select[name=country] :selected').data('mobile_code'));
                $('input[name=country_code]').val($('select[name=country] :selected').data('code'));
                $('.mobile-code').text('+' + $('select[name=country] :selected').data('mobile_code'));
            });
            $('input[name=mobile_code]').val($('select[name=country] :selected').data('mobile_code'));
            $('input[name=country_code]').val($('select[name=country] :selected').data('code'));
            $('.mobile-code').text('+' + $('select[name=country] :selected').data('mobile_code'));
            $('.checkUser').on('focusout', function(e) {
                var url = '{{ route('user.checkUser') }}';
                var value = $(this).val();
                var token = '{{ csrf_token() }}';
                if ($(this).attr('name') == 'mobile') {
                    var mobile = `${$('.mobile-code').text().substr(1)}${value}`;
                    var data = {
                        mobile: mobile,
                        _token: token
                    }
                }
                if ($(this).attr('name') == 'email') {
                    var data = {
                        email: value,
                        _token: token
                    }
                }
                if ($(this).attr('name') == 'username') {
                    var data = {
                        username: value,
                        _token: token
                    }
                }
                $.post(url, data, function(response) {
                    if (response.data != false && response.type == 'email') {
                        $('#existModalCenter').modal('show');
                    } else if (response.data != false) {
                        $(`.${response.type}Exist`).text(`${response.type} already exist`);
                    } else {
                        $(`.${response.type}Exist`).text('');
                    }
                });
            });
        })(jQuery);
    </script>
@endpush
