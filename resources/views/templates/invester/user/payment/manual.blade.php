@extends($activeTemplate.'layouts.masterinner')
@section('content')
  <div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">

        <!-- Content Title & Slogan Start -->
        <div class="sub-pages-top-content-section ">
            <div class="top">
                <div class="d-flex justify-content-center align-items-center change-image-theme">
                    <img src="/transport/img/icons/transaction-verification.svg" alt="User Image">
                    <img src="/transport/img/icons/transaction-verification-dark-theme.svg" class="dark-theme-image" alt="User Image">
                </div>
                <div class="title mt-2">
                    Top up with {{ $data->gateway->name }}
                </div>
            </div>
            <div class="slogan text-align-center">
                @php echo  $data->gateway->description @endphp.
            </div>
        </div>
        <!-- Content Title & Slogan End -->

        <!-- Devide Start -->
        <div class="devider-without-line"></div>
        <!-- Devide End -->



<form action="{{ route('user.deposit.manual.update') }}" method="POST" enctype="multipart/form-data">
                        @csrf
        <!-- Authentication Pages Start -->
        <div class="pages-list_title mb-4">
            <div class="pages-list">
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title-light ml-0">
                            AMOUNT TO PAY
                        </div>
                        <div class="arrow-right arrow-text-1 ml-auto">
                            {{showAmount($data['final_amo']) .' '.$data['method_currency'] }}
                        </div>
                    </div>
                    <a href="#" class="page-list_item-page-link"></a>
                </div>
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title-light ml-0">
                            WITH
                        </div>
                        @if($data->gateway->name=='AIRTEL MONEY')
                        <div class="arrow-right arrow-text-1 ml-auto">
                             {{ $data->gateway->name }}
                        </div>
                        @else

                        <div class="arrow-right arrow-text-1 ml-auto">
                             {{ $data->gateway->name }}
                        </div>
                        @endif
                    </div>
                    <a href="#" class="page-list_item-page-link"></a>
                </div>
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        @if($data->gateway->name=='AIRTEL MONEY')
                        <div style="color: RED;" class="page-title-light ml-0">
                        
                        </div>
                        @else
                         <div style="color: RED;" class="page-title-light ml-0">
                         
                        </div>
                        @endif
                        <div class="arrow-right arrow-text-1 ml-auto">
                            
                        </div>
                    </div>
                    <a href="#" class="page-list_item-page-link"></a>
                </div>
                <div class="item-page">
                    <x-viser-form identifier="id" identifierValue="{{ $gateway->form_id }}" />
                </div>
                

            </div>
        </div>
        <!-- Authentication Pages End -->
          <!-- Devide Start -->
          <div class="devider-without-line left"></div>
          <!-- Devide End -->
          <div class="modal-bottom d-flex mt-4">
              <a href="{{ url('user.dashboard') }}" class="btn btn-light w-100" data-dismiss="modal">CANCEL</a>
              <button  class="btn btn-success bg-color-success w-100 ml-2">SEND</button>
          </div>
      </div>
  </form>
      <!-- All Content End -->
    </div>
@endsection


