@extends($activeTemplate.'layouts.masterinner')

@section('content')

<script>
    // Set the date we're counting down to
   
    <?php

$db_date = date('F d, Y H:i:s',strtotime($deposit->created_at));

?>
var countDownDate = new Date("<?php echo $db_date;?>").getTime() + 30 * 60 * 1000;

    
    // Update the count down every 1 second
    var x = setInterval(function() {
    
      // Get today's date and time
      var now = new Date().getTime();
    
      console.log(now);
    
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
    
      // Time calculations for hours, minutes and seconds
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
      // Display the result in the element with id="demo"
      document.getElementById("demo").innerHTML =  hours + ":"
      + minutes + ":" + seconds;
    
      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("timer").innerHTML = "<h4 class='text-danger'>Transaction time out. KIndly try out some other time!</h4>";
      }
    }, 1000);
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="main-content p-100 text-center" id="status">
      <!-- All Content Start -->
      <div class="content-padding" id="timer">

        <!-- Content Title & Slogan Start -->
        <div class="sub-pages-top-content-section ">
            <div class="top">
                <div class="d-flex justify-content-center align-items-center change-image-theme">
                    <img src="img/icons/transaction-received.svg" alt="User Image">
                    <img width="50" src="/transport/img/usdt.svg" class="dark-theme-image" alt="">
                </div>
                <div class="title mt-2 text-center">
                    Instant USDT Payment
                </div>
            </div>
            <div class="mt-1 text-center">
               <span class="text-primary">Send Exactly <span class="text-success">{{ $data->amount }}</span>  {{__($data->currency)}} To</span>
            </div>
            <div class="mt-1 text-center">
   <span class="text-success" id="sendToAddress">{{ $data->sendto }}</span>
   <button class="btn btn-warning btn-sm" id="copyBtn">Copy Address</button>
</div>

<script>
  document.getElementById("copyBtn").addEventListener("click", function() {
    var sendToAddress = document.getElementById("sendToAddress");
    var textArea = document.createElement("textarea");
    textArea.value = sendToAddress.textContent;
    document.body.appendChild(textArea);
    textArea.select();
    document.execCommand("copy");
    textArea.remove();
    alert("Copied: " + sendToAddress.textContent);
  });
</script>
        </div>
        <!-- Content Title & Slogan End -->





     
            
               <img src="{{$data->img}}" alt="@lang('Image')" class="text-center">
                
                    <div class="mt-1 text-center">
               <span class="text-primary">Pay Within</span> <span class="text-warning" id="demo"></span>
                    </div>
                         
                    
                   
              

        


      </div>
      <!-- All Content End -->
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   


    <script>
        $(document).ready(function(){
          setInterval(function(){
            $.ajax({
              url: "{{ route('custom.deposit_check') }}",
              type: "GET",
              success: function(data) {
                if(data.status == 1)
                {
                    window.location.href = "{{ route('user.deposit.history') }}"

                }
                
              },
              error: function(data) {
                console.log(data);
              }
            });
          }, 2000);
        });
        </script>

        
    
@endsection