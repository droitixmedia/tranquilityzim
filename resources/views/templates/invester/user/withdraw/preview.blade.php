@extends($activeTemplate.'layouts.masterinner')
@section('content')

<div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">

        <!-- Content Title & Slogan Start -->
        <div class="sub-pages-top-content-section">
            <div class="d-flex align-items-center">
                <div class="title-image mr-2 change-image-theme">
                    <img src="img/icons/my-cards.svg" class="d-block" alt="Paperwork Icon">
                    <img src="/transport/img/icons/my-cards-dark-theme.svg" class="dark-theme-image" alt="Paperwork Icon">
                </div>
                <div class="title">
                   Confirm Your Details
                </div>
            </div>
            <div class="slogan">
                Please check your details before submitting
            </div>
            <!--<div class="add-button mt-3 mb-3">

                <div class="title">
                    + Add a new card

                </div>

            </div>-->
        </div>
        <!-- Content Title & Slogan End -->

        
          <!-- Devide Start -->
          <div class="devider-without-line left mb-2"></div>
          <!-- Devide End -->
      <form class="card-form" action="{{route('user.withdraw.submit')}}" method="post" enctype="multipart/form-data">
        @csrf
        
          
          

          
            <div class="card-name">
                <x-viser-form identifier="id" identifierValue="{{ $withdraw->method->form_id }}" />

            </div>

            <button type="submit" class="btn btn-primary">Submit</button>

      </form>


              <!-- Form Field End -->




          <!-- Transactions Block End -->
        



        
      </div>
      <!-- All Content End -->
    </div>
@endsection


