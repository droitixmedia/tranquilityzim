@extends($activeTemplate . 'layouts.masterinner')
@section('content')
<!-- Content Title & Slogan Start -->
    <div class="sub-pages-top-content-section top-box-padding">
        <div class="d-flex align-items-center">
            <div class="title-image mr-2 change-image-theme">
                <img src="/transport/img/icons/transactions.svg" class="d-block" alt="Paperwork Icon">
                <img src="/transport/img/icons/transactions-dark-theme.svg" class="dark-theme-image" alt="Paperwork Icon">
            </div>
            <div class="title">
                Withdrawal Transactions
            </div>
        </div>
        <div class="slogan">
            All done withdrawals and pending.
        </div>
        <!-- Devide Start -->
        <div class="devider-without-line mb-2"></div>
        <!-- Devide End -->
    </div>
    <div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">


          <!-- Transactions Block Start -->
          <div class="transactions-block information-block mt-110">
              
              @forelse($withdraws as $withdraw)
              @if ($withdraw->status == 2)
              <a href="#" class="item-transaction d-flex align-items-center">
                  <div class="item-transaction_transaction-image d-flex justify-content-center align-items-center">
                    @if($withdraw->method->name =='INNBUCKS')
                      <img width="40" src="/transport/img/dollar.svg" class="border-50" alt="Transaction Image">
                     @elseif($withdraw->method->name =='ECOCASH USD')
                     <img width="40" src="/transport/img/dollar.svg" class="border-50" alt="Transaction Image">
                     @else
                     <img width="40" src="/transport/img/USDT.png" class="border-50" alt="Transaction Image">
                     @endif 
                  </div>
                  <div class="item-transaction_transaction-title">
                      {{ __($withdraw->method->name) }} WITHDRAWAL
                      <div class="transaction-type">
                          ..Pending
                      </div>
                      <div class="transaction-type">
                          {{ showDateTime($withdraw->created_at) }}
                      </div>
                  </div>
                  <div class="item-transaction_transaction-amount ml-auto negative">
                      - {{ showAmount($withdraw->amount-$withdraw->charge) }} {{ __($general->cur_text) }}
                  </div>
                  
              </a>
              @elseif($withdraw->status == 1)
               <a href="#" class="item-transaction d-flex align-items-center">
                  <div class="item-transaction_transaction-image d-flex justify-content-center align-items-center">
                   
                      <img width="40" src="/transport/img/icons/approved-dark-theme.svg" class="border-50" alt="Transaction Image">
                 
                  </div>
                  <div class="item-transaction_transaction-title">
                      {{ __($withdraw->method->name) }} WITHDRAWAL
                      <div class="transaction-type">
                          Complete
                      </div>
                      <div class="transaction-type">
                          {{ showDateTime($withdraw->created_at) }}
                      </div>
                  </div>
                  <div style="color: green;" class="item-transaction_transaction-amount ml-auto">
                      -{{ showAmount($withdraw->amount-$withdraw->charge) }} {{ __($general->cur_text) }}
                  </div>
                  
              </a>
              @elseif($withdraw->status == 3)
              <a href="#" class="item-transaction d-flex align-items-center">
                  <div class="item-transaction_transaction-image d-flex justify-content-center align-items-center">
                   
                      <img width="40" src="/transport/img/icons/rejected-dark-theme.svg" class="border-50" alt="Transaction Image">
                 
                  </div>
                  <div class="item-transaction_transaction-title">
                      {{ __($withdraw->method->name) }} DEPOSIT
                      <div class="transaction-type">
                          Failed
                      </div>
                      <div class="transaction-type">
                          Withdraw Rejected
                      </div>
                  </div>
                  <div style="color: red;" class="item-transaction_transaction-amount ml-auto">
                      {{ showAmount($withdraw->amount-$withdraw->charge) }} {{ __($general->cur_text) }}
                  </div>
                  
              </a>
              @else

              @endif
              @empty

              <div   style="display: flex; justify-content: center; align-items: center;">
                      <img width="70" style="align-content: center;" src="/transport/img/icons/rejected.svg" class="border-50" alt="Transaction Image"><br>

              </div>
              @endforelse
              
            
          </div>
         <h5 class="total-price mb-0"> {{ $withdraws->links() }}</h5>
      </div>
      <!-- All Content End -->
    </div>
    <script>
  document.querySelector('form').addEventListener('submit', function (event) {
    event.preventDefault();

    fetch(this.action, {
      method: this.method,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'X-CSRF-TOKEN': document.querySelector('input[name="_token"]').value
      }
    })
    .then(response => {
      if (!response.ok) {
        throw new Error(response.statusText);
      }

      window.location.replace('/');
    })
    .catch(error => {
      console.error(error);
    });
  });
</script>
@endsection



