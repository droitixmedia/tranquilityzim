@extends($activeTemplate.'layouts.masterinner')
@section('content')
@php
use App\Models\Withdrawal;
@endphp
@php
$user = auth()->user();
@endphp
@php

$WithdrawalsToday = Withdrawal::where('user_id', $user->id)
    ->whereDate('created_at', today())
    ->count();
@endphp
<div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">

        <!-- Content Title & Slogan Start -->
        <div class="sub-pages-top-content-section">
            <div class="d-flex align-items-center">
                <div class="title-image mr-2 change-image-theme">
                    <img src="img/icons/my-cards.svg" class="d-block" alt="Paperwork Icon">
                    <img src="/transport/img/icons/my-cards-dark-theme.svg" class="dark-theme-image" alt="Paperwork Icon">
                </div>
                <div class="title">
                    Withdraw Cash
                </div>
            </div>
            <div class="slogan">
                Withdraw from your account.
            </div>
            <!--<div class="add-button mt-3 mb-3">

                <div class="title">
                    + Add a new card

                </div>

            </div>-->
        </div>
        <!-- Content Title & Slogan End -->

        
          <!-- Devide Start -->
          <div class="devider-without-line left mb-2"></div><br><br>
          <!-- Devide End -->
        
      <form class="card-form"  action="{{route('user.withdraw.money')}}" method="post">
         @csrf
          <label>Select Method</label>
          <select class="form-control form--control form-select" name="method_code" required>
                                
                                @foreach($withdrawMethod as $data)
                                    <option value="{{ $data->id }}" data-resource="{{$data}}">  {{__($data->name)}}</option>
                                @endforeach
                            </select>

          
            <div class="card-name">
                <label>Enter Amount</label>
                <input class="form-control"  type="number" step="any" name="amount" value="{{ old('amount') }}" placeholder="Enter Amount" aria-label=".form-control-lg example">

            </div>

            <button type="submit" class="btn btn-primary">Withdraw</button>

      </form>
   


              <!-- Form Field End -->




          <!-- Transactions Block End -->
        



        
      </div>
      <!-- All Content End -->
    </div>
@endsection

@push('script')
<script>
    (function ($) {
        "use strict";
        $('select[name=method_code]').change(function(){
            if(!$('select[name=method_code]').val()){
                $('.preview-details').addClass('d-none');
                return false;
            }
            var resource = $('select[name=method_code] option:selected').data('resource');
            var fixed_charge = parseFloat(resource.fixed_charge);
            var percent_charge = parseFloat(resource.percent_charge);
            var rate = parseFloat(resource.rate)
            var toFixedDigit = 2;
            $('.min').text(parseFloat(resource.min_limit).toFixed(2));
            $('.max').text(parseFloat(resource.max_limit).toFixed(2));
            var amount = parseFloat($('input[name=amount]').val());
            if (!amount) {
                amount = 0;
            }
            if(amount <= 0){
                $('.preview-details').addClass('d-none');
                return false;
            }
            $('.preview-details').removeClass('d-none');

            var charge = parseFloat(fixed_charge + (amount * percent_charge / 100)).toFixed(2);
            $('.charge').text(charge);
            if (resource.currency != '{{ $general->cur_text }}') {
                var rateElement = `<span>@lang('Conversion Rate')</span> <span class="fw-bold">1 {{__($general->cur_text)}} = <span class="rate">${rate}</span>  <span class="base-currency">${resource.currency}</span></span>`;
                $('.rate-element').html(rateElement);
                $('.rate-element').removeClass('d-none');
                $('.in-site-cur').removeClass('d-none');
                $('.rate-element').addClass('d-flex');
                $('.in-site-cur').addClass('d-flex');
            }else{
                $('.rate-element').html('')
                $('.rate-element').addClass('d-none');
                $('.in-site-cur').addClass('d-none');
                $('.rate-element').removeClass('d-flex');
                $('.in-site-cur').removeClass('d-flex');
            }
            var receivable = parseFloat((parseFloat(amount) - parseFloat(charge))).toFixed(2);
            $('.receivable').text(receivable);
            var final_amo = parseFloat(parseFloat(receivable)*rate).toFixed(toFixedDigit);
            $('.final_amo').text(final_amo);
            $('.base-currency').text(resource.currency);
            $('.method_currency').text(resource.currency);
            $('input[name=amount]').on('input');
        });
        $('input[name=amount]').on('input',function(){
            var data = $('select[name=method_code]').change();
            $('.amount').text(parseFloat($(this).val()).toFixed(2));
        });
    })(jQuery);
</script>
@endpush
