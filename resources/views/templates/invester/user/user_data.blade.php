@extends($activeTemplate . 'layouts.appauth')
@section('panel')
   
  

    <div class="main-content h-100">
      <!-- All Content Start -->
      <div class="content-padding h-100 d-flex flex-column justify-content-center min-600">

        <!-- Logo FinHit Start -->
        <div class="text-center d-flexjustify-content-center align-items-center mt-auto change-image-theme-ib">
          <img src="/transport/img/icons/finger-touch.svg" alt="FinHit Logo">
          <img src="/transport/img/icons/finger-touch.svg" class="dark-theme-image" alt="FinHit Logo">

          <!-- Slogan Start -->
          <div class="slogan text-center">User Data</div>
          <!-- Slogan End -->

        </div>
        <!-- Logo FinHit End -->

        <!-- Sign Up Form Start -->
        <form id="userdata" class="sing-up-form" action="{{ route('user.data.submit') }}" method="POST">
            @csrf
 
            <div>
                <input class="form-control" type="text" autocomplete="off" name="firstname" value="{{ old('firstname') }}" placeholder="Enter Your Name">
            </div>
           
            <div>
                <input class="form-control" type="text" autocomplete="off" name="lastname" value="{{ old('lastname') }}" placeholder="Enter Your Surname">
            </div>
                  
        
            <div>
                <input class="form-control"  type="text" type="text" name="mobile" placeholder="0973464663" value="{{ old('mobile') }}">
            </div>
           

            
 

      

        </form><br>
        <!-- Sign Up Form End -->

       
        
        
        <button onclick="document.getElementById('userdata').submit()" class="btn btn-light">Finalize</button>

    
        
    
        <!-- Copyright Block Start -->
        <div class="copyright-block text-center mt-auto">
          Copyright &copy; <span>myTransport</span><span></span> 2023. All Rights Reserved.
        </div>
        <!-- Copyright Block End -->
        
      </div>
      <!-- All Content End -->
    </div>
@endsection




