@extends($activeTemplate.'layouts.mastertrans')
@section('content')

@php
use App\Models\Deposit;
@endphp

@php
$user_id = auth()->id();
@endphp

@php

$deposit = Deposit::where('user_id', $user_id)
                  ->where('status', 2)
                  ->first()
@endphp

@php
$balance = auth()->user()->deposit_wallet;
@endphp
              

    <div class="main-content m-100">

      <!-- All Content Start -->
      <div class="content-padding">

        <!-- Total Balance Add Money Start -->
        <div class="d-flex justify-content-between align-items-end total-balance-add-money-block">
            <div class="total-balance-block">
                 
                <div class="money-balance">
                    ${{ showAmount($balance) }}
                </div>
            </div>
            <a href="#" class="add-money-block text-center change-image-theme">
                <img src="/transport/img/icons/add-money-icon.svg" alt="Add Money Icon">
                <img src="/transport/img/pic.png" width="60" class="dark-theme-image" alt="Money">
                  <style>
  #copyBtn {
    font-size: 12px;
    padding: 4px 8px;
  }
</style>
                  <div class="coupon-form">
  <input class="form-control" type="text" id="myInput" value="{{ route('user.register') }}?reference={{ auth()->user()->username }}" readonly style="display: none;">
  <button class="btn btn-primary btn-sm" id="copyBtn">Copy Your Link</button>
</div>

<script>
  var copyBtn = document.getElementById("copyBtn");
  var input = document.getElementById("myInput");

  copyBtn.addEventListener("click", function() {
    input.style.display = "block";
    input.select();
    document.execCommand("copy");
    input.style.display = "none";
    alert("Copied the referral link to clipboard!");
  });
</script>
            </a>
        </div>
        <!-- Total Balance Add Money End -->

        <!-- Popups Menu Block Start -->
        <div class="popups-menu-block d-flex justify-content-between align-items-center">
            <a href="{{ route('user.withdraw') }}" class="popup-menu-item text-center">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/assets-protection.svg" alt="Withdraw Image">
                    <img src="/transport/img/icons/assets-protection-dark-theme.svg" class="dark-theme-image" alt="Withdraw Image">
                </div>
                Withdraw
            </a>
            <div class="popup-menu-item text-center" data-toggle="modal" data-target="#sendMoneyModal">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/budget-balance.svg" alt="Send Image">
                    <img src="/transport/img/icons/budget-balance-dark-theme.svg" class="dark-theme-image" alt="Send Image">
                </div>
                Deposit
            </div>
            <a href="{{ route('user.moneycenter') }}" class="popup-menu-item text-center">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/exchange.svg" alt="Exchange Image">
                     <img src="/transport/img/icons/add-money-icon-dark-theme.svg" class="dark-theme-image" alt="Money">
                </div>
                Money Hall
            </a>
            <div class="popup-menu-item text-center" data-toggle="modal" data-target="#supportModal">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/support.svg" alt="Support Image">
                    <img src="/transport/img/icons/support-dark-theme.svg" class="dark-theme-image" alt="Support Image">
                </div>
                Support
            </div>
             
        </div>
        <!-- Popups Menu Block End -->


         <!-- Popups Menu Block Start -->
        <div class="popups-menu-block d-flex justify-content-between align-items-center">
            <a href="https://chat.whatsapp.com/LuDNjFCYAHL9XV7l0M2IXy" class="popup-menu-item text-center">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/assets-protection.svg" alt="Withdraw Image">
                    <img width="30" src="/transport/img/whatsapp.svg" class="dark-theme-image" alt="Withdraw Image">
                </div>
                Join Our Whatsapp Group
            </a>
            <a href="https://t.me/+GebzMPW4B4VhYTdk" class="popup-menu-item text-center" >
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/budget-balance.svg" alt="Send Image">
                    <img width="35" src="/transport/img/telegram.svg" class="dark-theme-image" alt="Send Image">
                </div>
                Join Our Telegram Group
            </a>
            <a href="{{ route('user.referrals') }}" class="popup-menu-item text-center">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/exchange.svg" alt="Exchange Image">
                     <img width="30" src="/transport/img/bonus.svg" class="dark-theme-image" alt="Money">
                </div>
                View Your Downliners
            </a>
            <a href="{{ url('user/deposit/history') }}" class="popup-menu-item text-center">
                <div class="image-wrapper change-image-theme d-flex justify-content-center align-items-center">
                    <img src="/transport/img/icons/exchange.svg" alt="Exchange Image">
                     <img width="30" src="/transport/img/deposit.svg" class="dark-theme-image" alt="Money">
                </div>
                Check Deposit History
            </a>
            
        </div>
        <!-- Popups Menu Block End -->
        <style>
            .ribbon {
  position: absolute;
  right: -5px;
  top: -5px;
  z-index: 1;
  overflow: hidden;
  width: 75px;
  height: 75px;
  text-align: right;
}

.ribbon span {
  font-size: 10px;
  font-weight: bold;
  color: #FFF;
  text-transform: uppercase;
  text-align: center;
  line-height: 20px;
  transform: rotate(45deg);
  width: 100px;
  display: block;
  background: #79A70A;
  background: linear-gradient(#9BC90D 0%, #79A70A 100%);
  box-shadow: 0 3px 10px -5px rgba(0, 0, 0, 1);
  position: absolute;
  top: 19px;
  right: -21px;
}

.ribbon span::before {
  content: "";
  position: absolute;
  left: 0px;
  top: 100%;
  z-index: -1;
  border-left: 3px solid #79A70A;
  border-right: 3px solid transparent;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #79A70A;
}

.ribbon span::after {
  content: "";
  position: absolute;
  right: 0px;
  top: 100%;
  z-index: -1;
  border-left: 3px solid transparent;
  border-right: 3px solid #79A70A;
  border-bottom: 3px solid transparent;
  border-top: 3px solid #79A70A;
}

        </style>
        
        <!-- Devide Start -->
        <div class="devider right"></div>
        <!-- Devide End -->
         <style>

        .item-transaction_transaction-image img {
            animation: spin 3s linear infinite;
        }

        @keyframes spin {
            from {
                transform: rotateY(0deg);
            }
            to {
                transform: rotateY(360deg);
            }
        }
    </style>
        <div class="transactions-block information-block mt-3">
           
<style>
    .item-transaction_transaction-amount {
    display: inline-block;
    overflow: hidden;
    font-size: 32px;
    font-weight: bold;
    line-height: 1;
    text-align: center;
}

.item-transaction_transaction-amount > span {
    display: inline-block;
    width: 50px;
    height: 70px;
    margin-right: 5px;
    background-color: #fff;
    color: #000;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    transform-origin: bottom;
    transition: transform 0.5s ease-in-out;
    animation: flip 1s ease-in-out infinite alternate;
}

.item-transaction_transaction-amount > span:last-child {
    margin-right: 0;
}

.item-transaction_transaction-amount > span.current {
    transform: rotateX(0deg);
}

.item-transaction_transaction-amount > span.next {
    transform: rotateX(-90deg);
}

@keyframes flip {
    from {
        transform: rotateX(0deg);
    }
    to {
        transform: rotateX(-90deg);
    }
}

</style>
            
            
 <script>
      function updateTimer() {
        var currentTime = new Date();
        currentTime.setHours(currentTime.getUTCHours() + 2); // offset to SAST
        var hours = currentTime.getHours();
        var minutes = currentTime.getMinutes();
        var seconds = currentTime.getSeconds();

        if (hours == 10 && minutes >= 0 && minutes < 5) {
          document.getElementById("timer").innerHTML = "Open";
        } else if (hours == 14 && minutes >= 0 && minutes < 5) {
          document.getElementById("timer").innerHTML = "Open";
        } else if (hours == 19 && minutes >= 0 && minutes < 5) {
          document.getElementById("timer").innerHTML = "Open";
        } else {
          if (hours < 9) {
  var timeLeft = (9 - hours) * 60 * 60 + (60 - minutes) * 60 - seconds;
} else if (hours < 13) {
  var timeLeft = (13 - hours) * 60 * 60 + (60 - minutes) * 60 - seconds;
} else if (hours < 18) {
  var timeLeft = (18 - hours) * 60 * 60 + (60 - minutes) * 60 - seconds;
} else {
  var timeLeft = (24 - hours + 9) * 60 * 60 + (60 - minutes) * 60 - seconds;
}

          var hoursLeft = Math.floor(timeLeft / 3600);
          var minutesLeft = Math.floor((timeLeft % 3600) / 60);
          var secondsLeft = timeLeft % 60;

          document.getElementById("hours").innerHTML = hoursLeft;
          document.getElementById("minutes").innerHTML = minutesLeft;
          document.getElementById("seconds").innerHTML = secondsLeft;
        }
      }

      setInterval(updateTimer, 1000); // Update timer every second
    </script>



 <!-- MIDDLE Modal Support Start-->
    <div class="modal fade" id="earn-modal" tabindex="-1" role="dialog" aria-labelledby="supportModalTitle" aria-hidden="true">
        <div class="modal-dialog mx-auto my-0 d-flex h-100 pt-5" role="document">
            <div class="modal-content popup-win mt-auto">

                <div class="modal-middle mt-4">
                    <div class="text-center mt-3 change-image-theme-ib">
                        <img src="/transport/img/icons/approved.svg" class="modal-icon" alt="Support Icon">
                        <img src="/transport/img/icons/rejected-dark-theme.svg" class="dark-theme-image modal-icon" alt="Support Icon">
                        <div class="modal-approve-title pt-3">
                            MEGA EARN WILL START AT 10AM ZAMBIAN TIME , ON 14 FEBRUARY!!
                        </div>
                    </div>


                </div>
                <div class="modal-bottom d-flex mt-4 justify-content-center mb-3">
                    <a href="{{ url('/user/dashboard') }}"  class="btn btn-light w-50 br-50 b-1">Go Home!</a>
                </div>
            </div>
        </div>
    </div>



                
           
    

         <!-- Monthly BilLs Block Start -->
        <div class="monthly-bills-block information-block mt-3">
            <div class="information-block_title d-flex justify-content-between align-items-center mb-3">
                <div>
                    PLANS
                </div>
                <a href="#" class="text-underline">View All</a>
            </div>
            <div class="bills-carousel owl-carousel">
                <div class="item-bill text-center change-image-theme">
                    <img src="/transport/img/icons/water-bill.svg" class="bill-icon" alt="Water Bill">
                    <img src="/transport/img/dollar.svg"  class="bill-icon dark-theme-image" alt="Water Bill">
                    <div class="ribbon">
                    <span>40% in 4 Days</span>
                   </div>
                    <div class="bill-ammount">
                        4 Days Plan
                    </div>
                    <div class="bill-title">
                       Earn 10% Daily for 4 Days
                    </div>
                  
                    <a href="{{ route('level1') }}" class="btn btn-success btn-pay">Open Now</a>
                    
                </div>
                <div class="item-bill text-center change-image-theme">
  <img src="/transport/img/icons/energy-consumption.svg" class="bill-icon" alt="Water Bill">
  <img src="/transport/img/dollar.svg" class="bill-icon dark-theme-image" alt="energy-consumption">
  <div class="ribbon">
    <span style="background: purple;">70% in 7 Days</span>
  </div>
  <div class="bill-ammount">
    7 Days Plan
  </div>
  <div class="bill-title">
    Earn 10% daily for 7 Days
  </div>
 
 <a href="{{ route('level2') }}" class="btn btn-success btn-pay">Open Now</a>
 
</div>

                
            </div>
        </div>
        <!-- Monthly BilLs Block End -->
         <!-- Monthly BilLs Block Start -->
        <div style="margin-bottom: 40px;" class="monthly-bills-block information-block mt-3">
            
            <div class="bills-carousel owl-carousel">
                

               
                <div class="item-bill text-center change-image-theme">
                    <img src="/transport/img/icons/mobile_bill.svg" class="bill-icon" alt="Water Bill">
                    <img src="/transport/img/dollar.svg" class="bill-icon dark-theme-image" alt="Water Bill">
                    <div class="ribbon">
                     <span style="background: blue;">150% in 15 Days</span>
                    </div>
                    <div class="bill-ammount">
                        15 Days Plan
                    </div>
                    <div class="bill-title">
                        Earn 10% Daily for 15 Days
                    </div>
                   
                    <a href="{{ route('level3') }}" class="btn btn-success btn-pay">Open Now</a>
                   
                </div>
                <div class="item-bill text-center change-image-theme">
                    <img src="/transport/img/icons/Insurance_bill.svg" class="bill-icon" alt="Insurance Bill">
                    <img src="/transport/img/dollar.svg" class="bill-icon dark-theme-image" alt="Insurance Bill">
                    <div  class="ribbon">
                    <span style="background: red;">300% in 30 Days</span>
                    </div>
                    <div class="bill-ammount">
                        30 Days Plan
                    </div>
                    <div class="bill-title">
                        Earn 10% Daily for 30 Days
                    </div>
                    
                    <a href="{{ route('level4') }}" class="btn btn-success btn-pay">Open Now</a>
                
                </div>
            </div>
        </div>
        <!-- Monthly BilLs Block End -->

        
        <!-- Devide End -->
        

        <!-- Devide Start -->
        <div class="devider left mt-4"></div>
        <!-- Devide End -->

           <!-- Copyright Block Start -->
<div class="copyright-block copyright-big text-center">
    <div class="top-block">
        Copyright &copy; <span>Tranquility</span><span>TFS</span> 2023. All Rights Reserved.
    </div>
     Shares.
</div>
<!-- Copyright Block End -->    


        
      </div>
      <!-- All Content End -->
    </div>  

@endsection

@push('script')

                                      
                                          <script>
        (function ($) {
            "use strict";
            $('select[name=gateway]').change(function(){
                if(!$('select[name=gateway]').val()){
                    $('.preview-details').addClass('d-none');
                    return false;
                }
                var resource = $('select[name=gateway] option:selected').data('gateway');
                var fixed_charge = parseFloat(resource.fixed_charge);
                var percent_charge = parseFloat(resource.percent_charge);
                var rate = parseFloat(resource.rate)
                if(resource.method.crypto == 1){
                    var toFixedDigit = 8;
                    $('.crypto_currency').removeClass('d-none');
                }else{
                    var toFixedDigit = 2;
                    $('.crypto_currency').addClass('d-none');
                }
                $('.min').text(parseFloat(resource.min_amount).toFixed(2));
                $('.max').text(parseFloat(resource.max_amount).toFixed(2));
                var amount = parseFloat($('input[name=amount]').val());
                if (!amount) {
                    amount = 0;
                }
                if(amount <= 0){
                    $('.preview-details').addClass('d-none');
                    return false;
                }
                $('.preview-details').removeClass('d-none');
                var charge = parseFloat(fixed_charge + (amount * percent_charge / 100)).toFixed(2);
                $('.charge').text(charge);
                var payable = parseFloat((parseFloat(amount) + parseFloat(charge))).toFixed(2);
                $('.payable').text(payable);
                var final_amo = (parseFloat((parseFloat(amount) + parseFloat(charge)))*rate).toFixed(toFixedDigit);
                $('.final_amo').text(final_amo);
                if (resource.currency != '{{ $general->cur_text }}') {
                    var rateElement = `<span class="fw-bold">@lang('Conversion Rate')</span> <span><span  class="fw-bold">1 {{__($general->cur_text)}} = <span class="rate">${rate}</span>  <span class="method_currency">${resource.currency}</span></span></span>`;
                    $('.rate-element').html(rateElement)
                    $('.rate-element').removeClass('d-none');
                    $('.in-site-cur').removeClass('d-none');
                    $('.rate-element').addClass('d-flex');
                    $('.in-site-cur').addClass('d-flex');
                }else{
                    $('.rate-element').html('')
                    $('.rate-element').addClass('d-none');
                    $('.in-site-cur').addClass('d-none');
                    $('.rate-element').removeClass('d-flex');
                    $('.in-site-cur').removeClass('d-flex');
                }
                $('.method_currency').text(resource.currency);
                $('input[name=currency]').val(resource.currency);
                $('input[name=method_code]').val(resource.method_code);
                $('input[name=amount]').on('input');
            });
            $('input[name=amount]').on('input',function(){
                $('select[name=gateway]').change();
                $('.amount').text(parseFloat($(this).val()).toFixed(2));
            });
        })(jQuery);
    </script>
                                          
    
@endpush
