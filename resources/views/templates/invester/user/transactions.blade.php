@extends($activeTemplate.'layouts.masterinner')
@section('content')


    <div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">

        <!-- Content Title & Slogan Start -->
        <div class="sub-pages-top-content-section">
            <div class="d-flex align-items-center">
                <div class="title-image mr-2">
                    <img src="/transport/img/icons/notification-icon.svg" class="d-block" alt="Paperwork Icon">
                </div>
                <div class="title">
                    All Transactions
                </div>
            </div>
            <div class="slogan">
                Check all your system transactions.
            </div>
        </div>
        <!-- Content Title & Slogan End -->

        <!-- Devide Start -->
        <div class="devider-without-line"></div>
        <!-- Devide End -->

          <!-- Transactions Block Start -->
          @forelse($transactions as $transaction)
          <div class="finhit-notification-block information-block mt-4">
              
              <div class="item-transaction d-flex align-items-center">
                  <div class="item-transaction_transaction-image d-flex justify-content-center align-items-center change-image-theme-ib">
                      <img src="img/icons/down-arrow.svg" alt="">
                      @if($transaction->trx_type == '-')
                      <img src="/transport/img/icons/down-arrow-dark-theme.svg" class="dark-theme-image" alt="">
                      @else
                       <img src="/transport/img/icons/notification-right-arrow-dark-theme.svg" class="dark-theme-image" alt=""> 
                      @endif
                  </div>
                  <a href="#" class="item-transaction_transaction-title">
                      {{ ucwords(str_replace('_', ' ', $transaction->remark)) }}

                      <div class="transaction-type">
                          {{ __($transaction->details) }}
                      </div>
                      <div class="transaction-third-type">
                      {{showDateTime($transaction->created_at)}}
                      </div>
                  </a>
                  <div style="margin-left: 10px;" class="arrow-right ml-auto change-image-theme-ib">
                      
                    <a href="#">${{ showAmount($transaction->amount) }} </a>
                  </div>
              </div>
              
          </div>


          <!-- Transactions Block End -->
        @empty
          <div   style="display: flex; justify-content: center; align-items: center;">
                      <img width="70" style="align-content: center;" src="/transport/img/icons/rejected.svg" class="border-50" alt="Transaction Image"><br>

              </div>

         @endforelse

           <h5 style="background-color:red;" class="total-price mb-0"> {{ $transactions->links() }}</h5>

        
      </div>
      <!-- All Content End -->
    </div>
@endsection

