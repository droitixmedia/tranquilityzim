@extends($activeTemplate.'layouts.masterinner')
@section('content')

<div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">

        <!-- Content Title & Slogan Start -->
        <div class="sub-pages-top-content-section ">
            <div class="top">
                <div class="user-image-wrapper d-flex justify-content-center align-items-center">
                    <img src="/transport/img/pic.png" class="user-image-wrapper border-50" alt="User Image">
                </div>
                <div class="title">
                    {{ $user->fullname }}
                </div>
            </div>
            <div class="slogan">
                Control your account and manage Data.
            </div>
        </div>
        <!-- Content Title & Slogan End -->

        <!-- Devide Start -->
        <div class="devider-without-line"></div>
        <!-- Devide End -->

          
        

      

        <!-- Authentication Pages Start -->
        <div class="pages-list_title mb-4">
            <div class="title">
                PROFILE SETTINGS
            </div>
            <div class="pages-list">
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title ml-0">
                            Change Username
                        </div>
                        <div class="arrow-right ml-auto">
                            <img src="/transport/img/icons/arrow-right.svg" alt=""><img class="ml-2 dark-theme-image" src="/transport/img/icons/arrow-right-dark-theme.svg" alt="">
                        </div>
                    </div>
                    <a href="{{ route('user.profile.setting') }}" class="page-list_item-page-link"></a>
                </div>
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title ml-0">
                            Update Phone Number
                        </div>
                        <div class="arrow-right ml-auto">
                            <img src="/transport/img/icons/arrow-right.svg" alt=""><img class="ml-2 dark-theme-image" src="/transport/img/icons/arrow-right-dark-theme.svg" alt="">
                        </div>
                    </div>
                    <a href="{{ route('user.profile.setting') }}" class="page-list_item-page-link"></a>
                </div>
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title ml-0">
                            Change Names
                        </div>
                        <div class="arrow-right ml-auto">
                            <img src="/transport/img/icons/arrow-right.svg" alt=""><img class="ml-2 dark-theme-image" src="/transport/img/icons/arrow-right-dark-theme.svg" alt="">
                        </div>
                    </div>
                    <a href="{{ route('user.profile.setting') }}" class="page-list_item-page-link"></a>
                </div>
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title ml-0">
                            Allow Notifications
                        </div>
                        <div class="arrow-right ml-auto d-flex align-items-center">
                            <input class="apple-switch" type="checkbox" checked>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Authentication Pages End -->

        <!-- Blog Pages Start -->
        <div class="pages-list_title mb-4">
            <div class="title">
                SECURITY
            </div>
            <div class="pages-list">
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title ml-0">
                            Change Password
                        </div>
                        <div class="arrow-right ml-auto">
                            <img src="/transport/img/icons/arrow-right.svg" alt=""><img class="ml-2 dark-theme-image" src="/transport/img/icons/arrow-right-dark-theme.svg" alt="">
                        </div>
                    </div>
                    <a href="{{ route('user.change.password') }}" class="page-list_item-page-link"></a>
                </div>
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title ml-0">
                           2FA Authentication
                        </div>
                        <div class="arrow-right ml-auto d-flex align-items-center">
                            <input class="apple-switch" type="checkbox">
                        </div>
                    </div>
                </div>
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title ml-0">
                            Log Out All Devices
                        </div>
                        <div class="arrow-right ml-auto">
                            <img src="/transport/img/icons/arrow-right.svg" alt=""><img class="ml-2 dark-theme-image" src="/transport/img/icons/arrow-right-dark-theme.svg" alt="">
                        </div>
                    </div>
                    <a href="{{ route('user.logout') }}" class="page-list_item-page-link"></a>
                </div>
            </div>
        </div>
        <!-- Blog Pages End -->

      </div>
      <!-- All Content End -->
    </div>

@endsection
