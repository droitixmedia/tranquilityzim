<!doctype html>
<html lang="en">


<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
  <link rel="stylesheet" href="/transport/css/main.min.css" type="text/css" media="screen">
  <link rel="shortcut icon" href="#">
  <title>{{ $general->siteName(__($pageTitle)) }}</title>
</head>

<body class="worldwide-bg dark-theme load">

    @yield('panel')


    @stack('script')

    @include('partials.plugins')

    @include('partials.notify')

  
    
    <script type="text/javascript" src="/transport/js/main.min.js"></script>
</body>

</html>