@extends($activeTemplate.'layouts.apptrans')
@section('panel')
@guest

@else
@include($activeTemplate.'modals.deposit')
@include($activeTemplate.'modals.exchange')
@include($activeTemplate.'modals.withdraw')
@include($activeTemplate.'modals.send')
@include($activeTemplate.'modals.support')
@endguest

@include($activeTemplate.'partials.topbarinner')

@yield('content')

@include($activeTemplate.'partials.footertrans')




@endsection
