<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="/transport/css/main.min.css" type="text/css" media="screen">

  <link rel="shortcut icon" href="#">
  <title>Go Home</title>
    

    @stack('style')

</head>

<body class="light-grey overview-page dark-theme load">
   
@yield('panel')

  <script src="{{ asset('assets/global/js/jquery-3.6.0.min.js') }}"></script>
    
@stack('script')
@include('partials.plugins')
@include('partials.notify') 
    
     <!-- Footer Start -->
 

    @stack('script-lib')

   




<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="/transport/js/main.min.js"></script>
</body>


</html>