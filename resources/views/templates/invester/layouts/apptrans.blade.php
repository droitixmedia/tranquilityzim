<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="/transport/css/main.min.css" type="text/css" media="screen">
<script src="https://kit.fontawesome.com/891a82a9f3.js" crossorigin="anonymous"></script>
  <link rel="shortcut icon" href="#">
  <title> {{ $general->siteName(__($pageTitle)) }}</title>
    @include('partials.seo')

    @stack('style')

</head>

<body class="light-grey overview-page dark-theme load">

<!-- Modals Start -->

<!-- Modals End -->

<!-- Header Start -->

<!-- Header End -->    
@yield('panel')

  <script src="{{ asset('assets/global/js/jquery-3.6.0.min.js') }}"></script>
    
@stack('script')
@include('partials.plugins')
@include('partials.notify') 
    
     <!-- Footer Start -->
 

    @stack('script-lib')

   
<!-- Footer End -->
<script>
        $(".langSel").on("change", function() {
            window.location.href = "{{ route('home') }}/change/" + $(this).val();
        });

        Array.from(document.querySelectorAll('table')).forEach(table => {
            let heading = table.querySelectorAll('thead tr th');
            Array.from(table.querySelectorAll('tbody tr')).forEach((row) => {
                Array.from(row.querySelectorAll('td')).forEach((colum, i) => {
                    colum.setAttribute('data-label', heading[i].innerText)
                });
            });
        });

        $.each($('input, select, textarea'), function(i, element) {
            var elementType = $(element);
            if (elementType.attr('type') != 'checkbox') {
                if (element.hasAttribute('required')) {
                    $(element).closest('.form-group').find('label').addClass('required');
                }
            }
        });

        var inputElements = $('[type=text],[type=password],[type=email],[type=number],select,textarea');
        $.each(inputElements, function(index, element) {
            element = $(element);
            element.closest('.form-group').find('label').attr('for', element.attr('name'));
            element.attr('id', element.attr('name'))
        });

        $('.policy').on('click', function() {
            $.get('{{ route('cookie.accept') }}', function(response) {
                $('.cookies-card').addClass('d-none');
            });
        });


        setTimeout(function() {
            $('.cookies-card').removeClass('hide')
        }, 2000);
    </script>
    <script>
        function refreshPage() {
    window.location.href = window.location.href;
      }
    </script>



<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script type="text/javascript" src="/transport/js/main.min.js"></script>
</body>


</html>