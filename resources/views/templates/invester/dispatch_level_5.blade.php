@extends($activeTemplate.'layouts.masterinner')
@section('content')
 <div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">

      
        
        <!-- Devide End -->


              @foreach ($plans as $plan)
               <!-- Product Card -->
               @if (!auth()->user()->invests()->where('status', 1)->exists())
 @if ($investsToday >= 4)
<div class="card-inf-block change-image-theme">
            <span class="title">ATTENTION!</span>
            <span>Your Engagement liquidity for your level for today is exhausted!</span>
            <span class="title">Only 4 Allowed</span>
            <img class="w-65" src="/transport/img/icons/rejected.svg">
            <img class="w-65 dark-theme-image" src="/transport/img/icons/rejected-dark-theme.svg">
            <span class="title">Lets meet Tomorrow</span>
            <div class="d-flex mt-4 justify-content-center mb-3">
                <a href="{{ url('/user/dashboard') }}" class="btn btn-go w-145">or Upgrade for More!</a>
            </div>
        </div>
 @else
        <!-- Authentication Pages Start -->
        <div class="pages-list_title mb-4">
            
                        


<script>
  function displayTime() {
    var date = new Date();
    var time = date.toLocaleString();
    document.getElementById("time").innerHTML = time;
  }
  
  setInterval(displayTime, 1000);
</script>

                        
              
        <!-- Authentication Pages End -->
          <!-- Devide Start -->
          <div class="devider-without-line left"></div>
          <!-- Devide End -->
          <form id="play" action="{{ route('user.invest.submit') }}" method="post">
                           @csrf
                           <input type="hidden" name="plan_id" value="{{ $plan->id }}">
                           <div class="modal-body">
                              <div class="form-group">
                                 <h6 class="text-center investAmountRange"></h6>
                                 <p class="text-center mt-1 interestDetails"></p>
                                 <p class="text-center interestValidity"></p>
                                 <input type="hidden" name="wallet_type" value="deposit_wallet">
                                 <input type="hidden" name="name" value="{{ $randomEntry->name }}">
                                 <input type="hidden" name="surname" value="{{ $randomEntry->surname }}">
                                 <input type="hidden" name="company" value="{{ $randomEntry->company }}">
                                 <input type="hidden" name="picture" value="{{ $randomEntry->picture }}">
                                 <input type="hidden" name="amount" value="{{ intval(auth()->user()->deposit_wallet) }}">
                              </div>
                           </div>
                           
                        </form>
      <script>
  function showContainer() {
    var currentTime = new Date();
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();

    if (hours == 9 && minutes == 54 && seconds < 5 && !readCookie('containerShown')) {
      document.getElementById("container").style.display = "block";
      createCookie('containerShown', true, 1);
    } else {
      document.getElementById("container").style.display = "none";
    }
  }

  function createCookie(name, value, days) {
    var expires;
    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = "; expires=" + date.toGMTString();
    } else {
      expires = "";
    }
    document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
  }

  function readCookie(name) {
    var nameEQ = encodeURIComponent(name) + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') c = c.substring(1, c.length);
      if (c.indexOf(nameEQ) === 0) return decodeURIComponent(c.substring(nameEQ.length, c.length));
    }
    return null;
  }

  setInterval(showContainer, 1000); // Check every 1 second
</script>

    <script>
      function showContainer() {
        var currentTime = new Date();
        var hours = currentTime.getHours();
        var minutes = currentTime.getMinutes();

        if (!(hours == 10 || hours == 14 || hours == 19) || (minutes >= 5 && minutes < 60)) {
          document.getElementById("off").style.display = "block";
        } else {
          document.getElementById("off").style.display = "none";
        }
      }

      setInterval(showContainer, 1000); // Check every 1 second
    </script>
    <script>
      function updateTimer() {
        var currentTime = new Date();
        var hours = currentTime.getHours();
        var minutes = currentTime.getMinutes();
        var seconds = currentTime.getSeconds();

         if (hours < 9) {
  var timeLeft = (9 - hours) * 60 * 60 + (60 - minutes) * 60 - seconds;
} else if (hours < 13) {
  var timeLeft = (13 - hours) * 60 * 60 + (60 - minutes) * 60 - seconds;
} else if (hours < 18) {
  var timeLeft = (18 - hours) * 60 * 60 + (60 - minutes) * 60 - seconds;
} else {
  var timeLeft = (24 - hours + 9) * 60 * 60 + (60 - minutes) * 60 - seconds;
}

        var hoursLeft = Math.floor(timeLeft / 3600);
        var minutesLeft = Math.floor((timeLeft % 3600) / 60);
        var secondsLeft = timeLeft % 60;

        document.getElementById("hours").innerHTML = hoursLeft;
        document.getElementById("minutes").innerHTML = minutesLeft;
        document.getElementById("seconds").innerHTML = secondsLeft;
      }

      setInterval(updateTimer, 1000); // Update timer every second
    </script>
    <div id="off" style="display: none;">
      <div class="card-inf-block change-image-theme">
        <span class="title">MEGA EARN OPENS AT 10AM, 2PM AND 7PM</span>
        <span>ONLY 20 PEOPLE PER SLOT.</span>
        <img class="w-65 move" src="img/icons/approved.svg">
        <img class="w-65 dark-theme-image move" src="/transport/img/icons/rejected.svg">
      </div>
      <div id="timer" class="title">
      <span id="hours"></span> hours
      <span id="minutes"></span> minutes
      <span id="seconds"></span> seconds left until next MEGA EARN.
    </div>
    </div>

    <div id="container" style="display: none;">
          <div class="card-inf-block change-image-theme">
  <span class="title">WELCOME TO MEGA EARN</span>
  <span>PLEASE CLICK START AND BEGIN EARNING.</span>
  <img class="w-65 move" src="img/icons/approved.svg">
  <img class="w-65 dark-theme-image move" src="/transport/img/dollar.svg">
  
  <div class="d-flex mt-4 justify-content-center mb-3">
    <a   onclick="document.getElementById('play').submit()" class="btn btn-go">START RUN</a>
   

  </div>
</div>
   </div>       
<style>
    .item-transaction_transaction-amount {
    display: inline-block;
    overflow: hidden;
    font-size: 32px;
    font-weight: bold;
    line-height: 1;
    text-align: center;
}

.item-transaction_transaction-amount > span {
    display: inline-block;
    width: 50px;
    height: 70px;
    margin-right: 5px;
    background-color: #fff;
    color: #000;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    transform-origin: bottom;
    transition: transform 0.5s ease-in-out;
    animation: flip 1s ease-in-out infinite alternate;
}

.item-transaction_transaction-amount > span:last-child {
    margin-right: 0;
}

.item-transaction_transaction-amount > span.current {
    transform: rotateX(0deg);
}

.item-transaction_transaction-amount > span.next {
    transform: rotateX(-90deg);
}

@keyframes flip {
    from {
        transform: rotateX(0deg);
    }
    to {
        transform: rotateX(-90deg);
    }
}

</style>
          @endif
          @else
         
       
 <script>
setInterval(function() {
  location.reload();
}, 60000); // 60000 milliseconds = 60 seconds
</script>
<div class="item-transaction_transaction-image">
  <span class="title">MEGA EARN IN PROGRESS!</span>
  <span>Mega earn has started and earning shortly</span>
  <img class="w-65 move" src="img/icons/approved.svg">
  <img class="w-65 dark-theme-image move" src="/transport/img/icons/transaction-sent.svg">
  
  
</div>

<style>
  .move {
    animation: move 2s linear infinite;
  }

  @keyframes move {
    0% {
      transform: translateX(-100%);
    }
    50% {
      transform: translateX(100%);
    }
    100% {
      transform: translateX(-100%);
    }
  }
</style>


          @endif
          @endforeach
      </div>
      <!-- All Content End -->
    </div>
    @push('script')
<script>
   (function($){
       "use strict"
       $('.investModal').click(function(){
           var symbol = '{{ $general->cur_sym }}';
           var currency = '{{ $general->cur_text }}';
           $('.gateway-info').addClass('d-none');
           var modal = $('#investModal');
           var plan = $(this).data('plan');
           modal.find('[name=plan_id]').val(plan.id);
           modal.find('.planName').text(plan.name);
           let fixedAmount = parseFloat(plan.fixed_amount).toFixed(2);
           let minimumAmount = parseFloat(plan.minimum).toFixed(2);
           let maximumAmount = parseFloat(plan.maximum).toFixed(2);
           let interestAmount = parseFloat(plan.interest);

           if (plan.fixed_amount > 0) {
               modal.find('.investAmountRange').text(`Invest: ${symbol}${fixedAmount}`);
               modal.find('[name=amount]').val(parseFloat(plan.fixed_amount).toFixed(2));
               modal.find('[name=amount]').attr('readonly',true);
           }else{
               modal.find('.investAmountRange').text(`Invest: ${symbol}${minimumAmount} - ${symbol}${maximumAmount}`);
               modal.find('[name=amount]').val('');
               modal.find('[name=amount]').removeAttr('readonly');
           }

           if (plan.interest_type == '1') {
               modal.find('.interestDetails').html(`<strong> Interest: ${interestAmount}% </strong>`);
           } else {
               modal.find('.interestDetails').html(`<strong> Interest: ${interestAmount} ${currency}  </strong>`);
           }

           if (plan.lifetime == '0') {
               modal.find('.interestValidity').html(`<strong>  Every ${plan.time_name} for ${plan.repeat_time} times</strong>`);
           } else {
               modal.find('.interestValidity').html(`<strong>  Every ${plan.time_name} for life time </strong>`);
           }

       });

       $('[name=amount]').on('input',function(){
           $('[name=wallet_type]').trigger('change');
       })

       $('[name=wallet_type]').change(function () {
           var amount = $('[name=amount]').val();
           if($(this).val() != 'deposit_wallet' && $(this).val() != 'interest_wallet' && amount){
               var resource = $('select[name=wallet_type] option:selected').data('gateway');
               var fixed_charge = parseFloat(resource.fixed_charge);
               var percent_charge = parseFloat(resource.percent_charge);
               var charge = parseFloat(fixed_charge + (amount * percent_charge / 100)).toFixed(2);
               $('.charge').text(charge);
               $('.gateway-rate').text(parseFloat(resource.rate));
               $('.gateway-info').removeClass('d-none');
               if (resource.currency == '{{ $general->cur_text }}') {
                   $('.rate-info').addClass('d-none');
               }else{
                   $('.rate-info').removeClass('d-none');
               }
               $('.method_currency').text(resource.currency);
               $('.total').text(parseFloat(charge) + parseFloat(amount));
           }else{
               $('.gateway-info').addClass('d-none');
           }
       });
   })(jQuery);
</script>
@endpush
@endsection
