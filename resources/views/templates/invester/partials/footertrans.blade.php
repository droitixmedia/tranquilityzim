<div class="main-footer w-100">
    <div class="top-row"></div>        
    <div class="bottom-row d-flex justify-content-between align-items-center">

        <!-- Menu Item Start -->
        <div class="footer-menu-item text-center item-overview">
            
            <!-- Dark Theme Menu Item Start -->
            <a href="{{ route('user.home') }}" class="no-text-decoration dark-theme-menu-item">
                <img src="/transport/img/icons/overviews-dark-theme.svg" class="pasive" alt="Overview Icon">
                <img src="/transport/img/icons/overviews-active-dark-theme.svg" width="35" class="active" alt="Overview Active Icon">
                <div class="menu-title">
                    Dashboard
                </div>
            </a>
            <!-- Dark Theme Menu Item End -->
        </div>
        <!-- Menu Item End -->

        <!-- Menu Item Start -->
        <div class="footer-menu-item text-center item-pages">
           
            <!-- Dark Theme Menu Item Start -->
            <a href="{{ url('user/withdraw/history') }}" class="no-text-decoration dark-theme-menu-item">
               
                  <img width="30" src="/transport/img/withdrawal.svg" class="dark-theme-image" alt="Withdraw Image">
                <div class="menu-title">
                    Withdrawals
                    
                </div>
            </a>
            <!-- Dark Theme Menu Item End -->
        </div>
        <!-- Menu Item End -->

        <!-- Menu Item Start -->
        <div class="footer-menu-item text-center item-components">
            
            <!-- Dark Theme Menu Item Start -->
            <a href="{{ route('user.referrals') }}" class="no-text-decoration dark-theme-menu-item">
               
                 <img width="30" src="/transport/img/bonus.svg" class="dark-theme-image" alt="Send Image">
                <div class="menu-title">
                    Referrals
                </div>
            </a>
            <!-- Dark Theme Menu Item End -->
        </div>
        <!-- Menu Item End -->

        <!-- Menu Item Start -->
        <div class="footer-menu-item text-center item-cards">
            
            <!-- Dark Theme Menu Item Start -->
            <a data-toggle="modal" data-target="#transactionsModal" class="no-text-decoration dark-theme-menu-item">
                <img width="30" src="/transport/img/icons/transactions-dark-theme.svg" class="dark-theme-image" alt="Support Image">
                <div class="menu-title">
                    Transactions
                </div>
            </a>
            <!-- Dark Theme Menu Item End -->
        </div>
        <!-- Menu Item End -->

        <!-- Menu Item Start -->
        <div class="footer-menu-item text-center item-settings">
            <a href="{{ route('settings') }}" class="no-text-decoration">
                <img src="img/icons/profile-menu-icon.svg" alt="Profile Menu Icon">
                <div class="menu-title">
                    Account Setting
                </div>
            </a>
            <!-- Dark Theme Menu Item Start -->
            <a href="{{ route('settings') }}" class="no-text-decoration dark-theme-menu-item">
             
                <img src="/transport/img/icons/profile-menu-icon.svg" width="35" alt="Profile Menu Icon">
                <div class="menu-title">
                  Account Setting
                </div>
            </a>
            <!-- Dark Theme Menu Item End -->
        </div>
        <!-- Menu Item End -->

    </div>
</div>