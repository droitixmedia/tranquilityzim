<!-- Header Start -->
    <div class="main-header w-100">
        <div class="top-row d-flex justify-content-between align-items-center">
            
        </div>
        <div class="middle-row d-flex justify-content-between w-100 with-back-btn">

            <div class="d-flex align-items-center">
                <!-- Icon Back Start -->
                <div class="icon-back">
                    <a href="javascript: history.go(-1)">
                        <span></span>
                    </a>
                </div>
                <!-- Icon Back End -->

                
            </div>

            <!-- Page Title Start -->
            <div class="page-title">
                {{ $pageTitle }}
            </div>
            <!-- Page Title End -->

            <!-- Notification and User Block Start -->
            <div class="main-header_notification-user-block d-flex align-items-center">
                <a href="#" class="notification-block">
                    <img src="/transport/img/icons/notification.svg" alt="Notification Image">
                    <img src="/transport/img/icons/notification-dark-theme.svg" class="dark-theme-image" alt="Notification Image">
                    <div class="notification-number">0</div>
                </a>
              
            </div>
            <!-- Notification and User Block End -->
        </div>
        <div class="bottom-row">

        </div>
    </div>
    <!-- Header End -->