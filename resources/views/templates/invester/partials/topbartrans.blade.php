<div class="main-header w-100">
    <div class="top-row d-flex justify-content-between align-items-center">
        <div class="time">
            
        </div>
        <div class="mobile-info-icons">
            
        </div>
    </div>
    <div class="middle-row d-flex justify-content-between w-100">
        <!-- Menu Icon Start -->
        <div class="menu-icon icon-open">
           <a href="{{ route('user.logout') }}" class="logout-item">
                    <span class="logout-section_icon">
                        <i class="fal fa-sign-out fa-2x"></i>
                    </span>
                    
                </a>
        </div>
        <!-- Menu Icon End -->

        <!-- FinHit Logo Start -->
        <div class="finhit-logo">
            <img class="img-responsive" width="400" height="auto" src="/transport/img/Logo.png" alt="Transport Logo">
            <img class="img-responsive dark-theme-image"   src="/transport/img/logo.png" alt="Tranquility Logo">
        </div>
        <!-- FinHit Logo End -->

        <!-- Notification and User Block Start -->
        <div class="main-header_notification-user-block d-flex align-items-center">
            <a href="#" class="notification-block">
                <img src="/transport/img/icons/notification.svg"  alt="Notification Image">
                <img src="/transport/img/icons/notification-dark-theme.svg" width="35"  class="dark-theme-image" alt="Notification Image">
                <div class="notification-number">0</div>
            </a>
           
        </div>
        <!-- Notification and User Block End -->
    </div>
    <div class="bottom-row">

    </div>
</div>