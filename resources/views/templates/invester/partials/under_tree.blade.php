@php
use App\Models\Deposit;

@endphp

<ul @if($isFirst) class="firstList" @endif>
    @foreach($user->allReferrals as $under)
        @if($loop->first)
            @php $layer++ @endphp
        @endif

        @php
         $dep = Deposit::where('user_id', $under->id)->where('status', 1)->sum('amount');
        @endphp
        
        <div class="item-list d-flex align-items-center shadow-none br-05 bg-color justify-content-between pl-4 pr-4 fw-100 mt-2">
                  <div class="d-flex ff-2 fw-100">
                      <div class="user-image align-items-center mr-2">
                          <img src="/transport/img/user.png">
                      </div>
                     {{ $under->fullname }}
                  </div>
                  <span class="badge light-badge-20 br-dark-05 fw-400 fs-10 text-dark br-50"> ${{ showAmount($dep) }}</span>

              </div>
    @endforeach
</ul>
