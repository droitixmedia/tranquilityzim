@extends($activeTemplate.'layouts.masterinner')
@section('content')
 <div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">

       <!-- Content Title & Slogan Start -->
        <div class="sub-pages-top-content-section ">
            <div class="top mt-10">
                <div class="d-flex justify-content-center align-items-center mt-20 change-image-theme">
                    <img src="/transport/img/icons/faq.svg">
                    <img src="/transport/img/icons/budget-balance.svg" class="dark-theme-image">
                </div>
                <div class="title mt-2">
                    15 DAYS PLAN
                </div>
            </div>
            <div class="slogan text-center">
                Earn with the 15 Days Plan.
            </div>
        </div>
        <div class="charts-block information-block mt-3">
        
        <!-- Devide Start -->
        <div class="devider-without-line"></div>
        <!-- Devide End -->


              @foreach ($plans as $plan)
               <!-- Product Card -->
            
 
        <!-- Authentication Pages Start -->
        <div class="pages-list_title mb-4">
            <div class="pages-list">
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title-light ml-0">
                           15 DAYS PLAN
                        </div>




                        <div  class="arrow-right arrow-text-1 ml-auto">
                            150% in 15 DAYS
                        </div>
                    </div>
                    <a href="#" class="page-list_item-page-link"></a>
                </div>
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title-light ml-0">
                            Plan
                        </div>
                        <div class="arrow-right arrow-text-1 ml-auto">
                            <img width="30" src="/transport/img/dollar.svg" class="d-inline-block"> 15 DAYS
                        </div>
                    </div>
                    <a href="#" class="page-list_item-page-link"></a>
                </div>
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title-light ml-0">
                            Limits
                        </div>
                        <div class="arrow-right arrow-text-1 ml-auto">
                          $20 - $100000
                        </div>
                    </div>
                    <a href="#" class="page-list_item-page-link"></a>
                </div>
                
                
                
                <div class="item-page">
                    <div class="d-flex align-items-center">
                        <div class="page-title-light ml-0">
                           Earning
                        </div>
                        <div class="arrow-right arrow-text-1 fw-500 fs-15 text-black ml-auto">
                            Earnings will come after period
                        </div>
                    </div>
                    <a href="#" class="page-list_item-page-link"></a>
                </div>

            </div>
        </div>
        <!-- Authentication Pages End -->
          <!-- Devide Start -->
          <div class="devider-without-line left"></div>
          <!-- Devide End -->
          <form id="play" action="{{ route('user.invest.submit') }}" method="post">
                           @csrf
                           <input type="hidden" name="plan_id" value="{{ $plan->id }}">
                           <div class="modal-body">
                              <div class="form-group">
                                 <h6 class="text-center investAmountRange"></h6>
                                 <p class="text-center mt-1 interestDetails"></p>
                                 <p class="text-center interestValidity"></p>
                                 <input type="hidden" name="wallet_type" value="deposit_wallet">
                                 <input type="hidden" name="name" value="{{ $randomEntry->name }}">
                                 <input type="hidden" name="surname" value="{{ $randomEntry->surname }}">
                                 <input type="hidden" name="company" value="{{ $randomEntry->company }}">
                                 <input type="hidden" name="picture" value="{{ $randomEntry->picture }}">
                                 <input type="hidden" name="amount" value="{{ intval(auth()->user()->deposit_wallet) }}">
                              </div>
                           </div>
                           
                        </form>
          <div class="modal-bottom d-flex mt-4">
              
              <button class="btn btn-success bg-color-success w-100 ml-2" onclick="document.getElementById('play').submit()">Invest Now</button>
          </div>
     
          
          @endforeach
      </div>
      <!-- All Content End -->
    </div>
    @push('script')
<script>
   (function($){
       "use strict"
       $('.investModal').click(function(){
           var symbol = '{{ $general->cur_sym }}';
           var currency = '{{ $general->cur_text }}';
           $('.gateway-info').addClass('d-none');
           var modal = $('#investModal');
           var plan = $(this).data('plan');
           modal.find('[name=plan_id]').val(plan.id);
           modal.find('.planName').text(plan.name);
           let fixedAmount = parseFloat(plan.fixed_amount).toFixed(2);
           let minimumAmount = parseFloat(plan.minimum).toFixed(2);
           let maximumAmount = parseFloat(plan.maximum).toFixed(2);
           let interestAmount = parseFloat(plan.interest);

           if (plan.fixed_amount > 0) {
               modal.find('.investAmountRange').text(`Invest: ${symbol}${fixedAmount}`);
               modal.find('[name=amount]').val(parseFloat(plan.fixed_amount).toFixed(2));
               modal.find('[name=amount]').attr('readonly',true);
           }else{
               modal.find('.investAmountRange').text(`Invest: ${symbol}${minimumAmount} - ${symbol}${maximumAmount}`);
               modal.find('[name=amount]').val('');
               modal.find('[name=amount]').removeAttr('readonly');
           }

           if (plan.interest_type == '1') {
               modal.find('.interestDetails').html(`<strong> Interest: ${interestAmount}% </strong>`);
           } else {
               modal.find('.interestDetails').html(`<strong> Interest: ${interestAmount} ${currency}  </strong>`);
           }

           if (plan.lifetime == '0') {
               modal.find('.interestValidity').html(`<strong>  Every ${plan.time_name} for ${plan.repeat_time} times</strong>`);
           } else {
               modal.find('.interestValidity').html(`<strong>  Every ${plan.time_name} for life time </strong>`);
           }

       });

       $('[name=amount]').on('input',function(){
           $('[name=wallet_type]').trigger('change');
       })

       $('[name=wallet_type]').change(function () {
           var amount = $('[name=amount]').val();
           if($(this).val() != 'deposit_wallet' && $(this).val() != 'interest_wallet' && amount){
               var resource = $('select[name=wallet_type] option:selected').data('gateway');
               var fixed_charge = parseFloat(resource.fixed_charge);
               var percent_charge = parseFloat(resource.percent_charge);
               var charge = parseFloat(fixed_charge + (amount * percent_charge / 100)).toFixed(2);
               $('.charge').text(charge);
               $('.gateway-rate').text(parseFloat(resource.rate));
               $('.gateway-info').removeClass('d-none');
               if (resource.currency == '{{ $general->cur_text }}') {
                   $('.rate-info').addClass('d-none');
               }else{
                   $('.rate-info').removeClass('d-none');
               }
               $('.method_currency').text(resource.currency);
               $('.total').text(parseFloat(charge) + parseFloat(amount));
           }else{
               $('.gateway-info').addClass('d-none');
           }
       });
   })(jQuery);
</script>
@endpush
@endsection
