<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Gateway\PaymentController;

Route::get('/clear', function () {
    \Illuminate\Support\Facades\Artisan::call('optimize:clear');
});

Route::get('cron', 'CronController@cron')->name('cron');

// User Support Ticket
Route::controller('TicketController')->prefix('ticket')->name('ticket.')->group(function () {
    Route::get('/', 'supportTicket')->name('index');
    Route::get('new', 'openSupportTicket')->name('open');
    Route::post('create', 'storeSupportTicket')->name('store');
    Route::get('view/{ticket}', 'viewTicket')->name('view');
    Route::post('reply/{ticket}', 'replyTicket')->name('reply');
    Route::post('close/{ticket}', 'closeTicket')->name('close');
    Route::get('download/{ticket}', 'ticketDownload')->name('download');
});

Route::get('app/deposit/confirm/{hash}', 'Gateway\PaymentController@appDepositConfirm')->name('deposit.app.confirm');

Route::controller('LevelController')->group(function () {
    Route::get('level1', 'level1')->name('level1')->middleware('auth');
    Route::get('level2', 'level2')->name('level2')->middleware('auth');
    Route::get('level3', 'level3')->name('level3')->middleware('auth');
    Route::get('level4', 'level4')->name('level4')->middleware('auth');
    Route::get('level5', 'level5')->name('level5')->middleware('auth');
    Route::get('level6', 'level6')->name('level6')->middleware('auth');
  });
Route::controller('SiteController')->group(function () {
    Route::get('/contact', 'contact')->name('contact');
    Route::get('/settings', 'settings')->name('settings');
    Route::get('/support', 'support')->name('support')->middleware('auth');
    Route::get('/privacy', 'privacy')->name('privacy');
    Route::post('/contact', 'contactSubmit');
    Route::get('/change/{lang?}', 'changeLanguage')->name('lang');

    Route::get('cookie-policy', 'cookiePolicy')->name('cookie.policy');

    Route::get('/cookie/accept', 'cookieAccept')->name('cookie.accept');

    Route::get('blogs', 'blogs')->name('blogs');
    Route::get('blog/{slug}/{id}', 'blogDetails')->name('blog.details');

    Route::get('policy/{slug}/{id}', 'policyPages')->name('policy.pages');

    Route::get('plan', 'plan')->name('plan');
    Route::post('planCalculator', 'planCalculator')->name('planCalculator');

    Route::post('/subscribe', 'subscribe')->name('subscribe');

    Route::get('placeholder-image/{size}', 'placeholderImage')->name('placeholder.image');
    Route::post('/planCalculator', 'planCalculator')->name('planCalculator');

    Route::get('/{slug}', 'pages')->name('pages');
    Route::get('/', 'index')->name('home')->middleware('auth');
   
    //Route 
    Route::get('/api/deposit_check', [PaymentController::class, 'depositConfirmAjax'])->name('custom.deposit_check');


});
