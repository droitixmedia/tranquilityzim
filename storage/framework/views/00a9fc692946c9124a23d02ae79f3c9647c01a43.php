<?php $__env->startSection('content'); ?>
<div class="main-content p-100">
      <!-- All Content Start -->
      <div class="content-padding">

        <!-- Maintenance Content Start -->
        <div class="maintenance d-flex mt-110 change-image-theme">
            <img class="big-icon" src="/transport/img/icons/404.svg">
            <img class="big-icon dark-theme-image" src="/transport/img/icons/404-dark-theme.svg">
            <div class="maintenance-content">
                <span class="title">Page Not Found</span>
                <span class="text">This page could not be found, please go home.</span>
            </div>
            <a href="<?php echo e(url('/user/dashboard')); ?>" class="btn btn-light mt-5">GO HOME</a>
        </div>
        <!-- Maintenance Content End -->
      </div>
      <!-- All Content End -->
    </div>

<?php $__env->stopSection(); ?>



<?php echo $__env->make($activeTemplate.'layouts.mastererror', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/sites/tranquilityzim/resources/views/errors/404.blade.php ENDPATH**/ ?>