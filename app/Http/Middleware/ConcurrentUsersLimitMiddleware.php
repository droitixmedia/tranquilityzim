<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;

class ConcurrentUsersLimitMiddleware
{
    public $activeTemplate;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = 'concurrent_users_' . $request->path();
        $users = Cache::get($key, 0) + 1;

        if ($users > 0) {
            return view('templates.invester.exceed');
        }

        Cache::put($key, $users, 60);

        $response = $next($request);

        Cache::decrement($key);

        return $response;
    }
}
