<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Lib\FormProcessor;
use App\Lib\GoogleAuthenticator;
use App\Lib\HyipLab;
use App\Models\Deposit;
use App\Models\Form;
use App\Models\Invest;
use App\Models\PromotionTool;
use App\Models\Referral;
use App\Models\SupportTicket;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Withdrawal;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MoneyController extends Controller
{
    public function moneycenter()
    {
        $data['pageTitle']         = 'Statistics';
        $user                      = auth()->user();
        $today = Carbon::now()->startOfDay();
        $data['user']              = $user;
        $data['totalInvest']       = Invest::where('user_id', auth()->id())->sum('amount');
        $data['totalWithdraw']     = Withdrawal::where('user_id', $user->id)->whereIn('status', [1])->sum('amount');
        $data['lastWithdraw']      = Withdrawal::where('user_id', $user->id)->whereIn('status', [1])->latest()->first('amount');
        $data['totalDeposit']      = Deposit::where('user_id', $user->id)->where('status', 1)->sum('amount');
        $data['lastDeposit']       = Deposit::where('user_id', $user->id)->where('status', 1)->latest()->first('amount');
        $data['totalTicket']       = SupportTicket::where('user_id', $user->id)->count();
        $data['transactions']      = $data['user']->transactions->sortByDesc('id')->take(8);
        $data['referral_earnings'] = Transaction::where('remark', 'referral_commission')->where('user_id', auth()->id())->sum('amount');

        $data['submittedDeposits']  = Deposit::where('status', '!=', 0)->where('user_id', $user->id)->sum('amount');
        $data['successfulDeposits'] = Deposit::successful()->where('user_id', $user->id)->sum('amount');
        $data['requestedDeposits']  = Deposit::where('user_id', $user->id)->sum('amount');
        $data['initiatedDeposits']  = Deposit::initiated()->where('user_id', $user->id)->sum('amount');
        $data['pendingDeposits']    = Deposit::pending()->where('user_id', $user->id)->sum('amount');
        $data['rejectedDeposits']   = Deposit::rejected()->where('user_id', $user->id)->sum('amount');

        $data['submittedWithdrawals']  = Withdrawal::where('status', '!=', 0)->where('user_id', $user->id)->sum('amount');
        $data['successfulWithdrawals'] = Withdrawal::approved()->where('user_id', $user->id)->sum('amount');
        $data['rejectedWithdrawals']   = Withdrawal::rejected()->where('user_id', $user->id)->sum('amount');
        $data['initiatedWithdrawals']  = Withdrawal::initiated()->where('user_id', $user->id)->sum('amount');
        $data['requestedWithdrawals']  = Withdrawal::where('user_id', $user->id)->sum('amount');
        $data['pendingWithdrawals']    = Withdrawal::pending()->where('user_id', $user->id)->sum('amount');

        $data['invests']               = Invest::where('user_id', $user->id)->sum('amount');
        $data['completedInvests']      = Invest::where('user_id', $user->id)->where('status', 0)->sum('amount');
        $data['runningInvests']        = Invest::where('user_id', $user->id)->where('status', 1)->sum('amount');
        $data['interests']             = Transaction::where('remark', 'interest')->where('user_id', $user->id)->sum('amount');
        $data['depositWalletInvests']  = Invest::where('user_id', $user->id)->where('wallet_type', 'deposit_wallet')->where('status', 1)->sum('amount');
        $data['interestWalletInvests'] = Invest::where('user_id', $user->id)->where('wallet_type', 'interest_wallet')->where('status', 1)->sum('amount');

        $data['isHoliday']      = HyipLab::isHoliDay(now()->toDateTimeString(), gs());
        $data['nextWorkingDay'] = now()->toDateString();
        if ($data['isHoliday']) {
            $data['nextWorkingDay'] = HyipLab::nextWorkingDay(24);
            $data['nextWorkingDay'] = Carbon::parse($data['nextWorkingDay'])->toDateString();
        }

        $data['chartData'] = Transaction::where('remark', 'interest')
            ->where('created_at', '>=', Carbon::now()->subDays(30))
            ->where('user_id', $user->id)
            ->selectRaw("SUM(amount) as amount, DATE_FORMAT(created_at,'%Y-%m-%d') as date")
            ->orderBy('created_at', 'asc')
            ->groupBy('date')
            ->get();

            $data['shows'] = Transaction::whereIn('remark', ['deposit', 'withdraw', 'interest'])->orderBy('created_at', 'asc')->get();
            $data['totalEarningToday'] = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->sum('paid');
        $data['referral_earnings_today'] = Transaction::where('remark', 'referral_commission')->where('user_id', auth()->id())->where('created_at', '>=', $today)->sum('amount');


        return view($this->activeTemplate . 'user.center', $data);
    }


}
