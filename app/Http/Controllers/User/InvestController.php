<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Gateway\PaymentController;
use App\Lib\HyipLab;
use App\Models\GatewayCurrency;
use App\Models\Invest;
use App\Models\Plan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InvestController extends Controller
{
    public function invest(Request $request)
    {
        $request->validate([
            'amount'        => 'required|numeric|gt:19',
            'plan_id'       => 'required',
            'wallet_type'   => 'required',

        ]);
        $user   = auth()->user();
        $plan   = Plan::where('status',1)->findOrFail($request->plan_id);
        $amount = $request->amount;


         // Get the current date
        $today = Carbon::now()->startOfDay();

        // Count the number of transactions for the user today
        $transactionCount = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->count();

        if ($transactionCount >= 20) {

         $notify[] = ['error','You have reached your maximum dispatches for today'];
                return back()->withNotify($notify);
        }





        $wallet = $request->wallet_type;

        //Direct checkout
        if ($wallet != 'deposit_wallet' && $wallet != 'interest_wallet') {

            $gate = GatewayCurrency::whereHas('method', function ($gate) {
                $gate->where('status', 1);
            })->find($request->wallet_type);

            if (!$gate) {
                $notify[] = ['error', 'Invalid gateway'];
                return back()->withNotify($notify);
            }

            if ($gate->min_amount > $request->amount || $gate->max_amount < $request->amount) {
                $notify[] = ['error', 'Please follow deposit limit'];
                return back()->withNotify($notify);
            }

            $data = PaymentController::insertDeposit($gate,$request->amount,$plan);
            session()->put('Track', $data->trx);
            return to_route('user.deposit.confirm');
        }

        if ($request->amount > $user->$wallet) {
            $notify[] = ['error', 'Your balance is not sufficient'];
            return back()->withNotify($notify);
        }

        $hyip = new HyipLab($user, $plan);
        $hyip->invest($amount, $wallet);


    // Play the sound using JavaScript
    echo '<script>';
    echo "var audio = new Audio('/sounds/a.mp3');";
    echo 'audio.play();';
    echo '</script>';

        $notify[] = ['success','Order Succesfully Dispatched'];
        return back()->withNotify($notify);
    }

    public function statistics()
    {
        $pageTitle = 'Invest Statistics';
        $invests    = Invest::where('user_id',auth()->id())->orderBy('id','desc')->with('plan')->where('status',1)->paginate(getPaginate());
        $activePlan = Invest::where('user_id', auth()->id())->where('status', 1)->count();

        $investChart = Invest::where('user_id',auth()->id())->with('plan')->groupBy('plan_id')->select('plan_id')->selectRaw("SUM(amount) as investAmount")->orderBy('investAmount', 'desc')->get();
        return view($this->activeTemplate.'user.invest_statistics',compact('pageTitle','invests','investChart', 'activePlan'));
    }

    public function log()
    {
        $pageTitle = 'Invest Logs';
        $invests = Invest::where('user_id',auth()->id())->orderBy('id','desc')->with('plan')->paginate(getPaginate());
        return view($this->activeTemplate.'user.invests',compact('pageTitle','invests'));
    }
}
