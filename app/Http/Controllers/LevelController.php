<?php

namespace App\Http\Controllers;

use App\Models\AdminNotification;
use App\Models\Frontend;
use App\Models\GatewayCurrency;
use App\Models\Language;
use App\Models\Page;
use App\Models\Plan;
use App\Models\Driver;
use App\Models\Invest;
use App\Models\Subscriber;
use App\Models\SupportMessage;
use App\Models\SupportTicket;
use App\Models\TimeSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;

class LevelController extends Controller
{


    public function Level1()
    {
        $user   = auth()->user();
        $pageTitle       = "4 Days Plan";
        $plans           = Plan::where('status', 1)->where('level', 1)->get();
        $randomEntry = Driver::where('id', mt_rand(1, 5))->first();

        $view            = 'dispatch_level_1';
        $gatewayCurrency = 1;
        $invests = Invest::where('user_id',auth()->id())->orderBy('id','desc')->with('plan')->paginate(getPaginate());
          // Get the current date
        $today = Carbon::now()->startOfDay();

        // Count the number of transactions for the user today
        $investsToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->count();

        // Calculate the total balance of transactions for the user today
       $totalEarningToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->sum('paid');



        return view($this->activeTemplate . $view, compact('plans','pageTitle','gatewayCurrency','invests','investsToday','totalEarningToday','randomEntry'));
    }

    public function Level2()
    {
        $user   = auth()->user();
        $pageTitle       = "7 Days Plan";
        $plans           = Plan::where('status', 1)->where('level', 2)->get();
        $randomEntry = Driver::where('id', mt_rand(1, 5))->first();

        $view            = 'dispatch_level_2';
        $gatewayCurrency = 1;
        $invests = Invest::where('user_id',auth()->id())->orderBy('id','desc')->with('plan')->paginate(getPaginate());
          // Get the current date
        $today = Carbon::now()->startOfDay();

        // Count the number of transactions for the user today
        $investsToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->count();

        // Calculate the total balance of transactions for the user today
       $totalEarningToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->sum('paid');



        return view($this->activeTemplate . $view, compact('plans','pageTitle','gatewayCurrency','invests','investsToday','totalEarningToday','randomEntry'));
    }
    public function Level3()
    {
        $user   = auth()->user();
        $pageTitle       = "15 Days Plan";
        $plans           = Plan::where('status', 1)->where('level', 3)->get();
        $randomEntry = Driver::where('id', mt_rand(1, 5))->first();

        $view            = 'dispatch_level_3';
        $gatewayCurrency = 1;
        $invests = Invest::where('user_id',auth()->id())->orderBy('id','desc')->with('plan')->paginate(getPaginate());
          // Get the current date
        $today = Carbon::now()->startOfDay();

        // Count the number of transactions for the user today
        $investsToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->count();

        // Calculate the total balance of transactions for the user today
       $totalEarningToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->sum('paid');



        return view($this->activeTemplate . $view, compact('plans','pageTitle','gatewayCurrency','invests','investsToday','totalEarningToday','randomEntry'));
    }

    public function Level4()
    {
        $user   = auth()->user();
        $pageTitle       = "30 Days Plan";
        $plans           = Plan::where('status', 1)->where('level', 4)->get();
        $randomEntry = Driver::where('id', mt_rand(1, 5))->first();

        $view            = 'dispatch_level_4';
        $gatewayCurrency = 1;
        $invests = Invest::where('user_id',auth()->id())->orderBy('id','desc')->with('plan')->paginate(getPaginate());
          // Get the current date
        $today = Carbon::now()->startOfDay();

        // Count the number of transactions for the user today
        $investsToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->count();

        // Calculate the total balance of transactions for the user today
       $totalEarningToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->sum('paid');



        return view($this->activeTemplate . $view, compact('plans','pageTitle','gatewayCurrency','invests','investsToday','totalEarningToday','randomEntry'));
    }

     public function Level5()
    {
        $user   = auth()->user();
        $pageTitle       = "MEGA EARN";
        $plans           = Plan::where('status', 1)->where('level', 5)->get();
        $randomEntry = Driver::where('id', mt_rand(1, 5))->first();

        $view            = 'dispatch_level_5';
        $gatewayCurrency = 1;
        $invests = Invest::where('user_id',auth()->id())->orderBy('id','desc')->with('plan')->paginate(getPaginate());
          // Get the current date
        $today = Carbon::now()->startOfDay();

        // Count the number of transactions for the user today
        $investsToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->count();

        // Calculate the total balance of transactions for the user today
       $totalEarningToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->sum('paid');



        return view($this->activeTemplate . $view, compact('plans','pageTitle','gatewayCurrency','invests','investsToday','totalEarningToday','randomEntry'));
    }

   public function Level6()
    {
        $user   = auth()->user();
        $pageTitle       = "Shipping Level 6";
        $plans           = Plan::where('status', 1)->where('level', 6)->get();
        $randomEntry = Driver::where('id', mt_rand(1, 5))->first();

        $view            = 'dispatch_level_6';
        $gatewayCurrency = 1;
        $invests = Invest::where('user_id',auth()->id())->orderBy('id','desc')->with('plan')->paginate(getPaginate());
          // Get the current date
        $today = Carbon::now()->startOfDay();

        // Count the number of transactions for the user today
        $investsToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->count();

        // Calculate the total balance of transactions for the user today
       $totalEarningToday = Invest::where('user_id', $user->id)
        ->where('created_at', '>=', $today)
        ->sum('paid');



        return view($this->activeTemplate . $view, compact('plans','pageTitle','gatewayCurrency','invests','investsToday','totalEarningToday','randomEntry'));
    }




}
